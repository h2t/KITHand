# KITHand

This project offers low-level drivers as well as a grafical user interface to control any artifical hand developed at [H²T](https://h2t.anthropomatik.kit.edu/) from a PC. 
Depending on the abilities of a hand it can be controlled via Bluetooth LE and/or via a serial-to-usb adapter.

This library can be split into two parts:

- The low-level driver `KITHandCommunicationDriver` offers an abstract interface to all supported communication channels (currently serial and Bluetooth LE).
It encapsulates following functionalities:
    - Scanning for available hands
    - Connecting to found hand
    - Sending commands to a connected hand
    - Receiving data from a connected hand
    - Automatic parsing of received data and detection of the used communication protocol
- The high-level GUI `KITHandGUI` itself, which provides several extended functionalities related to hand communication. Such as:
    - Displaying sensor values
    - Controlling hand movements
    - 3D-View of current hand configuration


## Dependencies and Requirements

KITHand is written is C++ and has been developed for and tested on Ubuntu 18.04. Other operating systems are currently not supported.

It requires following packages: `pkgconf`, `build_essential`, `libpcre3-dev`, `libglib2.0-dev`, `libbluetooth-dev`, `bluez`. 
They can be installed with the following command:

```bash
sudo apt install pkgconf build_essential libpcre3-dev libglib2.0-dev libbluetooth-dev bluez
```

Following dependencies are required, but they will be downloaded and built locally if they are not installed on the system:
* [gattlib](https://github.com/labapart/gattlib)
* [boost](https://www.boost.org/)

The GUI is based on [Qt5](https://doc.qt.io/qt-5/) which can be installed by running:
```bash
sudo apt install qt5-default
```

To be able to use the 3D-Visualization you need to have [Simox](https://gitlab.com/Simox/simox), but this GUI-feature is optional and everything still works without it.

The user has to be in the group *dialout* to be allowed to access the serial ports of the system and therefore be able to communicate with a hand via the serial interface.
If this is not the case, run following command and log out and log in afterwards:
```bash
sudo usermod -a -G dialout userName
```

## Build step

```bash
git clone https://gitlab.com/h2t/KITHand KITHand
cd KITHand && mkdir build && cd build
cmake ..
make
```

Run the KITHandGUI:
```bash
cd bin
./KITHandGUI
```

### Install step

If you want to install KITHand run the following set of commands:
```bash
git clone https://gitlab.com/h2t/KITHand KITHand
cd KITHand && mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/path/to/install/dir ..
make && make install
```

### Documentation

This project has a documentation created with [doxygen](http://www.doxygen.nl/). It can be created by executing following command in the build-directory:
```bash
make && make doc
```

Afterwards the documentation can be found by opening
```bash
KITHand/build/doc/index.html
```

## Examples

[CommunicationExample](src/KITHandCommunicationDriver/examples/CommunicationExample.cpp): How to use the low-level driver to connect to a hand and send a few commands
```bash
./build/bin/CommunicationExample --scanmode bluetooth
```

[CommunicationWithCallbacksExample](src/KITHandCommunicationDriver/examples/CommunicationWithCallbacksExample.cpp): 
Similar example but this one demonstrates how to use callbacks to react to changes of the connection status and how to poll for sensor values
```bash
./build/bin/CommunicationWithCallbacksExample --scanmode both
```

## Known Issues

Depending on the configuration of your operating system, the connection to a connected hand might timeout after about 20 seconds. This is due to the fact that the bluetooth module in the hand tries to update the parameters of the connection and the operating system rejects this update.

To fix this issue you need to adapt the values *conn_min_interval* and *con_max_interval* for the used internal bluetooth device. The interval defined by these two values must include 16 as a lower and 32 as an upper bound. They can be edited either by executing
```bash
echo 16 | sudo tee /sys/kernel/debug/bluetooth/hci0/conn_min_interval
echo 32 | sudo tee /sys/kernel/debug/bluetooth/hci0/conn_max_interval
```
or by running
```bash
sudo hcitool lecup --min 16 --max 32 --handle 3585
```

> **_NOTE:_** Both values will be reset after a reboot or after the system awakes from sleep-mode
