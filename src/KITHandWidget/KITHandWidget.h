#pragma once

#include <QWidget>

#include <KITHandCommunicationDriver.h>
#ifdef KITHandWidget_WITH_ICE
#include <KITHandIceServer.h>
#endif

namespace Ui
{
    class KITHandWidget;
}

namespace KITHand
{
    /**
     * @brief Represents a very simple GUI for the KITHandGUI.
     */
    class KITHandWidget : public QWidget
    {
        Q_OBJECT
    public:
        /**
         * @brief Constructs a new KITHandWidget with the given \p parent.
         * @param parent The parent of this QWidget
         */
        explicit KITHandWidget(QWidget *parent = nullptr);
        ~KITHandWidget();

    protected:
        /**
         * @brief Timer event for updating the sensor vaules
         * @param event The QTimerEvent description
         */
        void timerEvent(QTimerEvent* event) override;

    private slots:
        void on_pushButtonSendRaw_released();
        void on_pushButtonConnect_released();
        void on_pushButtonSendGrasp_released();
        void on_pushButtonSendThumb_released();
        void on_pushButtonSendFingers_released();
        void on_pushButtonIceConnect_released();
        void on_pushButtonDisconnect_released();
        void on_pushButtonIceDisconnect_released();

    private:
        Ui::KITHandWidget *ui;
        #ifdef KITHandWidget_WITH_ICE
        KITHandIceServer _server;
        #endif
        KITHandCommunicationDriver _driver;
    };
}
