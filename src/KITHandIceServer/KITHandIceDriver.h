#pragma once

#include <KITHandCommunicationDriver.h>

#include "KITHandInterface.h"

namespace KITHand
{
    class KITHandIceDriver:
        virtual public KITHandInterface
    {
    private:
        KITHandCommunicationDriver  _driver;
        KITHandCommunicationDriver* _driverPtr;
        bool                            _externalDriver{false};

    public:
        KITHandIceDriver();
        ~KITHandIceDriver();

        KITHandCommunicationDriver& driver();

        void overrideLowlevelDriver(KITHandCommunicationDriver& driver);

        // KITHandInterface interface
    public:
        ConnectionStateIce::State connect(const ConnectionParameters& par = {}, const Ice::Current& = Ice::emptyCurrent) override;
        bool connected(const Ice::Current& = Ice::emptyCurrent) override;
        void disconnect(const Ice::Current& = Ice::emptyCurrent) override;

        void sendGrasp(Ice::Int n, const Ice::Current& = Ice::emptyCurrent) override;
        void sendThumbPWM(const MotorValues& values, const Ice::Current& = Ice::emptyCurrent) override;
        void sendFingersPWM(const MotorValues& values, const Ice::Current& = Ice::emptyCurrent) override;
        void sendRaw(const std::string& raw, const Ice::Current& = Ice::emptyCurrent) override;

        SensorValues getSensorValues(const Ice::Current& = Ice::emptyCurrent) override;
    };
}

