#include "KITHandIceDriver.h"

namespace KITHand
{

KITHandIceDriver::KITHandIceDriver() : _driverPtr{&_driver} {}

KITHandIceDriver::~KITHandIceDriver()
{
    if(!_externalDriver && connected())
    {
        disconnect();
    }
}

KITHandCommunicationDriver &KITHandIceDriver::driver()
{
    return *_driverPtr;
}

void KITHandIceDriver::overrideLowlevelDriver(KITHandCommunicationDriver &driver)
{
    if(_driver.connected())
    {
        throw std::logic_error{"Can't override the driver if the owned driver is connected!"};
    }
    _driverPtr = &driver;
    _externalDriver = true;
}

    ConnectionStateIce::State KITHandIceDriver::connect(
            const ConnectionParameters& par,
            const Ice::Current&)
    {
        auto p = KITHand::SensorProtocol::TposTpwmFposFpwm;
        switch(par.protocol)
        {
        case SensorProtocolIce::TposTpwmFposFpwm:
            p = KITHand::SensorProtocol::TposTpwmFposFpwm;
            break;
        case SensorProtocolIce::MxPosPwm:
            p = KITHand::SensorProtocol::MxPosPwm;
            break;
        }
//        KITHand::HandDevice device;
//        device.macAdress = par.mac;
//        device.protocol = p;
//        const auto r = _driverPtr->connect(device);
//        using State = KITHand::ConnectionState;
//        switch(r)
//        {
//            case State::Connected                            : return ConnectionStateIce::Connected;
//            case State::AreadyConnected                      : return ConnectionStateIce::AreadyConnected;

//            case State::FailedToFindReadCharacteristic       : return ConnectionStateIce::FailedToFindReadCharacteristic;
//            case State::FailedToFindWriteCharacteristic      : return ConnectionStateIce::FailedToFindWriteCharacteristic;
//            case State::FailedToFindReadWriteCharacteristics : return ConnectionStateIce::FailedToFindReadWriteCharacteristics;

//            case State::FailedToFindDevice                   : return ConnectionStateIce::FailedToFindDevice;
//            case State::FailedToConnectDevice                : return ConnectionStateIce::FailedToConnectDevice;

//            case State::FailedToOpenAdapter                  : return ConnectionStateIce::FailedToOpenAdapter;
//            case State::FailedToScanAdapter                  : return ConnectionStateIce::FailedToScanAdapter;

//            case State::ConnectedButNoSensorValues           : return ConnectionStateIce::ConnectedButNoSensorValues;
//        }
        throw std::logic_error{"unknown return value from connect"};
    }

    bool KITHandIceDriver::connected(const Ice::Current&)
    {
        return _driverPtr->connected();
    }

    void KITHandIceDriver::disconnect(const Ice::Current&)
    {
        _driverPtr->disconnect();
    }

    void KITHandIceDriver::sendGrasp(Ice::Int n, const Ice::Current&)
    {
        _driverPtr->sendGrasp(static_cast<std::uint64_t>(n));
    }

    void KITHandIceDriver::sendThumbPWM(const MotorValues& values, const Ice::Current&)
    {
        _driverPtr->sendThumbPWM(
            static_cast<std::uint64_t>(values.v),
            static_cast<std::uint64_t>(values.maxPWM),
            static_cast<std::uint64_t>(values.pos)
        );
    }

    void KITHandIceDriver::sendFingersPWM(const MotorValues&values, const Ice::Current&)
    {
        _driverPtr->sendFingersPWM(
            static_cast<std::uint64_t>(values.v),
            static_cast<std::uint64_t>(values.maxPWM),
            static_cast<std::uint64_t>(values.pos)
        );
    }

    void KITHandIceDriver::sendRaw(const std::string&raw, const Ice::Current&)
    {
        _driverPtr->sendRaw(raw);
    }

    SensorValues KITHandIceDriver::getSensorValues(const Ice::Current&)
    {
        SensorValues value;
        value.thumbPWM  = _driverPtr->getThumbPWM();
        value.thumbPos  = _driverPtr->getThumbPos();

        value.fingersPWM = _driverPtr->getFingersPWM();
        value.fingersPos = _driverPtr->getFingersPos();

        value.IMURoll = _driverPtr->getIMURoll();
        value.IMURoll = _driverPtr->getIMUPitch();
        value.IMURoll = _driverPtr->getIMUYaw();



        return value;
    }

}
