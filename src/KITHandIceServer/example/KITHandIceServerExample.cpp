#include <KITHandIceServer.h>
#include <KITHandGattlibDriver.h>

int main(int argc, char* argv[])
{
    KITHand::KITHandIceServer server;
    KITHand::ConnectionParameters p;
    p.mac = KITHand::MAC::V1;
    server.startIceServer(argc, argv);
    server.driver()->connect(p);
    server.waitForShutdown();
    return 0;
}
