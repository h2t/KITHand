if(BUILD_ICE_SERVER)
    find_package(Ice QUIET)

    if(NOT Ice_FOUND)
        message(STATUS "Ice not found! Not building ice server.")
    else()
        message(STATUS "Ice found! Building ice server.")
        message(STATUS "slice2cpp: ${Ice_slice2cpp}")

        set(slice_file "${CMAKE_CURRENT_LIST_DIR}/KITHandInterface.ice")
        add_custom_command(
            OUTPUT            KITHandInterface.h
                              KITHandInterface.cpp
            COMMAND           "${Ice_slice2cpp}" "${slice_file}"
            DEPENDS           "${slice_file}"
        )

        add_custom_target(
            KITHandInterface
            SOURCES
                "${CMAKE_CURRENT_LIST_DIR}/KITHandInterface.ice"
                "${CMAKE_CURRENT_BINARY_DIR}/KITHandInterface.h"
                "${CMAKE_CURRENT_BINARY_DIR}/KITHandInterface.cpp"
        )

        add_library(
            KITHandIceServer SHARED

            KITHandIceServer.cpp  KITHandIceServer.h
            KITHandIceDriver.cpp KITHandIceDriver.h
            #generated
            KITHandInterface.cpp KITHandInterface.h

        )

        add_dependencies(KITHandIceServer KITHandInterface)
        target_link_libraries(
            KITHandIceServer
            PUBLIC
                KITHandCommunicationDriver
                Ice
        )

        target_include_directories(
            KITHandIceServer
            PUBLIC
                $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
                $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
                $<INSTALL_INTERFACE:include/KITHand>
        )


        install(
            TARGETS KITHandIceServer
            EXPORT KITHandTargets
            LIBRARY DESTINATION lib
        )

        INSTALL(
            FILES
                KITHandIceServer.h
                KITHandIceDriver.h
                "${CMAKE_CURRENT_BINARY_DIR}/KITHandInterface.h"
                KITHandInterface.ice
            DESTINATION include/KITHand
        )
    endif()

    add_subdirectory(example)
else()
    message(STATUS "NOT building: KITHandIceServer")
endif()
