#include "Hand3DViewerWidget.h"
#include "../KITHandCommunicationDriver/KITHandCommunicationDriver.h"

#include <VirtualRobot/XML/RobotIO.h>

using namespace VirtualRobot;

namespace KITHand {
    Hand3DViewerWidget::Hand3DViewerWidget(QWidget *parent) : QWidget (parent)
    {
        QWidget* view1 = new QWidget(this);
        view1->setMinimumSize(100, 100);
        view1->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        SoQt::init(parent);
        _hand3DViewer = new Hand3DViewer(this);

        loadAndSetupHandVisualization();
    }

    Hand3DViewerWidget::~Hand3DViewerWidget()
    {
        delete _hand3DViewer;
    }

    void Hand3DViewerWidget::loadAndSetupHandVisualization()
    {
        std::string xmlPath = std::string(KITHandAdvancedWidget_DATA_PATH) + "/SimoxModels/RightHand_withDisplay/" + "prosthesis.xml";

        _hand = VirtualRobot::RobotIO::loadRobot(xmlPath);
        thumbProx = _hand->getRobotNode("thumb_prox");
        thumbDist = _hand->getRobotNode("thumb_dist");
        indexProx = _hand->getRobotNode("index_prox");
        indexDist = _hand->getRobotNode("index_dist");
        middleProx = _hand->getRobotNode("middle_prox");
        middleDist = _hand->getRobotNode("middle_dist");
        ringProx = _hand->getRobotNode("ring_prox");
        ringDist = _hand->getRobotNode("ring_dist");
        pinkyProx = _hand->getRobotNode("pinky_prox");
        pinkyDist = _hand->getRobotNode("pinky_dist");

        _handVisu = _hand->getVisualization<VirtualRobot::CoinVisualization>();
        _hand3DViewer->getRootNode()->addChild(_handVisu->getCoinVisualization());
        _hand3DViewer->show();
        _hand3DViewer->cameraViewAll();
    }

    void Hand3DViewerWidget::updateFingerPosition(long fingerPos)
    {
        float fractionClosed = static_cast<float>(fingerPos) / static_cast<float>(ControlOptions::maxPosFingers);
        float jointRange = 0;

        jointRange = indexProx->getJointLimitHigh() - indexProx->getJointLimitLow();
        indexProx->setJointValue(indexProx->getJointLimitLow() + jointRange * fractionClosed);
        jointRange = indexDist->getJointLimitHigh() - indexDist->getJointLimitLow();
        indexDist->setJointValue(indexDist->getJointLimitLow() + jointRange * fractionClosed);

        jointRange = middleProx->getJointLimitHigh() - middleProx->getJointLimitLow();
        middleProx->setJointValue(middleProx->getJointLimitLow() + jointRange * fractionClosed);
        jointRange = middleDist->getJointLimitHigh() - middleDist->getJointLimitLow();
        middleDist->setJointValue(middleDist->getJointLimitLow() + jointRange * fractionClosed);

        jointRange = ringProx->getJointLimitHigh() - indexProx->getJointLimitLow();
        ringProx->setJointValue(ringProx->getJointLimitLow() + jointRange * fractionClosed);
        jointRange = ringDist->getJointLimitHigh() - ringDist->getJointLimitLow();
        ringDist->setJointValue(indexDist->getJointLimitLow() + jointRange * fractionClosed);

        jointRange = pinkyProx->getJointLimitHigh() - pinkyProx->getJointLimitLow();
        pinkyProx->setJointValue(pinkyProx->getJointLimitLow() + jointRange * fractionClosed);
        jointRange = pinkyDist->getJointLimitHigh() - pinkyDist->getJointLimitLow();
        pinkyDist->setJointValue(pinkyDist->getJointLimitLow() + jointRange * fractionClosed);
    }

    void Hand3DViewerWidget::updateThumbPosition(long thumbPos)
    {
        float fractionClosed = static_cast<float>(thumbPos) / static_cast<float>(ControlOptions::maxPosThumb);
        float jointRange = 0;

        jointRange = thumbProx->getJointLimitHigh() - thumbProx->getJointLimitLow();
        thumbProx->setJointValue(thumbProx->getJointLimitLow() + jointRange * fractionClosed);

        jointRange = thumbDist->getJointLimitHigh() - thumbDist->getJointLimitLow();
        thumbDist->setJointValue(thumbDist->getJointLimitLow() + jointRange * fractionClosed);
    }
}


