#include "Hand3DViewer.h"

#include "Inventor/actions/SoLineHighlightRenderAction.h"
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>

namespace KITHand
{
    Hand3DViewer::Hand3DViewer(QWidget* parent) : SoQtExaminerViewer(parent, "", TRUE, SoQtExaminerViewer::BUILD_POPUP), sceneRootNode(new SoSeparator), contentRootNode(new SoSeparator), camera(new SoPerspectiveCamera)
    {
        this->setBackgroundColor(SbColor(150 / 255.0f, 150 / 255.0f, 150 / 255.0f));
        this->setAccumulationBuffer(true);
        this->setHeadlight(true);
        this->setViewing(false);
        this->setDecoration(false);

        this->setGLRenderAction(new SoLineHighlightRenderAction());
        this->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
        this->setFeedbackVisibility(true);
        this->setViewing(true);

        //Create scene root node
        sceneRootNode->ref();
        this->setSceneGraph(sceneRootNode);

        //Add camera to scene
        sceneRootNode->addChild(camera);
        this->setCamera(camera);

        //Give camera standard position
        camera->position.setValue(SbVec3f(10, -10, 5));
        camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));

        // Add contentNode
        sceneRootNode->addChild(contentRootNode);
    }

    Hand3DViewer::~Hand3DViewer()
    {
        sceneRootNode->unref();
    }

    SoSeparator *Hand3DViewer::getRootNode() const
    {
        return contentRootNode;
    }

    void Hand3DViewer::cameraViewAll()
    {
        camera->viewAll(this->contentRootNode, SbViewportRegion());
    }

    SbBool Hand3DViewer::processSoEvent(const SoEvent * const event)
    {
        const SoType type(event->getTypeId());

        // Process mouse press events
        if (type.isDerivedFrom(SoMouseButtonEvent::getClassTypeId()))
        {
            SoMouseButtonEvent* const ev = const_cast<SoMouseButtonEvent*>(static_cast<const SoMouseButtonEvent*>(event));
            const int button = ev->getButton();

            //LEFT AND MIDDLE MOUSE BUTTON
            if (button == SoMouseButtonEvent::BUTTON1 || button == SoMouseButtonEvent::BUTTON3)
            {
                SoQtExaminerViewer::processSoEvent(ev);
            }
            //RIGHT MOUSE BUTTON
            else if (button == SoMouseButtonEvent::BUTTON2)
            {
                // Disable right mouse button, becaus we dont want the user to able to open the popup-menu
                return TRUE;
            }


            // Keyboard handling
            if (type.isDerivedFrom(SoKeyboardEvent::getClassTypeId()))
            {
                const SoKeyboardEvent* const ev = static_cast<const SoKeyboardEvent* const>(event);

                /*The escape key and super key (windows key) is used to switch between
                viewing modes. We need to disable this behaviour completely.*/

                //65513 seems to be the super key, which is not available in the enum of keys in coin....
                if (ev->getKey() == SoKeyboardEvent::ESCAPE || ev->getKey() == 65513)
                {
                    return TRUE;
                }
                else if (ev->getKey() == SoKeyboardEvent::S && ev->getState() == SoButtonEvent::DOWN)
                {
                    if (!this->isSeekMode())
                    {
                        if (!this->isViewing())
                        {
                            this->setViewing(true);
                        }

                        SoQtExaminerViewer::processSoEvent(ev);
                        this->setSeekTime(0.5);
                        this->seekToPoint(ev->getPosition());

                        if (this->isViewing())
                        {
                            this->setViewing(false);
                        }
                    }
                }
                else
                {
                    SoQtExaminerViewer::processSoEvent(ev);
                }

                SoQtExaminerViewer::processSoEvent(ev);
            }
        }
        //Let all move events trough
        if (type.isDerivedFrom(SoLocation2Event::getClassTypeId()))
        {
            return SoQtExaminerViewer::processSoEvent(event);
        }

        //YOU SHALL NOT PASS!
        return TRUE;
    }
}


