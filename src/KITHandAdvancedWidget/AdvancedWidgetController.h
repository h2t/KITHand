#pragma once

#include <QObject>
#include <QThread>
#include <QTimer>

#include "KITHandCommunicationDriver.h"

namespace KITHand {
    /**
     * @brief Encapsulates the control of the underlying KITHandCommunicationDriver and uses signals to control the GUI.
     *
     * This class holds at all times an instance of a KITHandCommunicationDriver and a list of currently available hands.
     * It funtions as a controller (model) for the GUI (view).
     */
    class AdvancedWidgetController : public QObject
    {
        Q_OBJECT
    public:
        /**
         * @brief Constructs a new AdvancedWidgetController.
         *
         * Creates a new KITHandCommunicationDriver, registers a callback for the change of the connection state
         * and starts scanning for available hands.
         *
         * @param parent The parent of this QObject
         */
        explicit AdvancedWidgetController(QObject *parent = nullptr);
        ~AdvancedWidgetController() override;

    protected:
        /**
         * @brief Extends the timer event of this QObject to periodically process the new sensor values of the hand
         * @param event The QTimerEvent description
         */
        void timerEvent(QTimerEvent* event) override;

    signals:
        /**
         * @brief This signal is triggered when the state of the current connection has changed.
         * @param status A string representation of the new connection state
         */
        void updateConnectionStatus(QString status) const;
        /**
         * @brief This signal is triggered when scanning has finished.
         * @param deviceLabels A list of short descriptions of the found hands
         */
        void scanFinished(QStringList deviceLabels) const;
        /**
         * @brief This signal is triggered when the state of the current connecting changes and it
         * should now again or no longer be possible to connect to a hand.
         * @param allowed True if it is allowed to connect to hands, false if not
         */
        void connectingAllowed(bool allowed) const;

        // Signals for setting up gui according to currenct connected device
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param toggle True if connected hand reports PWM values
         */
        void togglePWMValuesAbility(bool toggle) const;
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param toggle True if connected hand reports IMU values
         */
        void toggleIMUValuesAbility(bool toggle) const;
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param toggle True if connected hand reports distance values
         */
        void toggleDistanceValuesAbility(bool toggle) const;
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param toggle True if connected hand supports additional grasp commands
         */
        void toggleAdditionalGraspsAbility(bool toggle) const;
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param toggle True if connected hand allows for changing the name of its bluetooth device
         */
        void toggleNameChangeAbility(bool toggle) const;
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param name The name of the connected hand
         */
        void setConnectedDeviceName(QString name) const;
        /**
         * @brief This signal is triggered when the gui connects to a hand and is setup according to
         * the hands abilites.
         * @param name The type of the connection (bluetooth or serial)
         */
        void setConnectionTypeName(QString name) const;

        /**
         * @brief This signal is triggered when new sensor values are available.
         * @param fingersPos The new position of the fingers motor in ticks
         * @param thumbPos The new position of the thumb motor in ticks
         */
        void updatePos(long fingersPos, long thumbPos) const;
        /**
         * @brief This signal is triggered when new sensor values are available.
         * @param fingersPWM The new PWM value of the fingers motor
         * @param thumbPWM The new PWM value of the thumb motor
         */
        void updatePWM(long fingersPWM, long thumbPWM) const;
        /**
         * @brief This signal is triggered when new sensor values are available.
         * @param roll The new roll value of the IMU
         * @param pitch The new pitch value of the IMU
         * @param yaw The new yaw value of the IMU
         */
        void updateIMU(long roll, long pitch, long yaw) const;
        /**
         * @brief This signal is triggered when new sensor values are available.
         * @param distance The new distance value of the Time-of-Flight sensor
         */
        void updateDistance(long distance) const;

        /**
         * @brief This signal is triggered when the process of connecting to a hand has finished.
         */
        void connectingFinished() const;
        /**
         * @brief This signal is triggered either when the disconnecting from the current
         * connection has finished or when the current hand is lost.
         */
        void disconnectingFinished() const;

    public slots:
        /**
         * @brief Repeatedly scans for available hands via every active communication channel
         *
         * This method runs indefinitely until this AdvancedWidgetController is destroyed.
         * Updates a list of the last found hands after every scan call and emits AdvancedWidgetController::scanFinished.
         */
        void scanForDevices();
        /**
         * @brief Tries to connect to the hand at the given index of the list of available hands.
         *
         * Emits AdvancedWidgetController::connectingFinished if connecting was successful, emits AdvancedWidgetController::disconnectingFinished
         * if connecting was not successful or the given index was invalid.
         *
         * @param index The index of the HandDevice to connect to
         */
        void connectToDeviceAtIndex(unsigned int index);
        /**
         * @brief Executed when the current connection should be disconnected.
         * Emits AdvancedWidgetController::updateConnectionStatus to indicate the disconnecting has started and
         * AdvancedWidgetController::disconnectingFinished once the connection is closed and the hand is disconnected.
         */
        void disconnectFromCurrentDevice();
        /**
         * @brief Executed when the button for sending raw data is released. Sends the given \p command to the KITHandCommunicationDriver.
         * @param command The raw data to send
         */
        void sendRaw(QString command) const;
        /**
         * @brief Executed when the button for changing the name of a hand is released. Attempts to change the name of
         * the bluetooth device on the currently connected hand.
         *
         * The \p newName cannot be longer than 32 characters.
         *
         * @param newName The new name
         */
        void changeDeviceName(QString newName) const;
        /**
         * @brief Sends a command with the given \p position to the thumb motor of the hand.
         * @param position The new thumb position in motor ticks
         */
        void setThumbMotorPosition(int position) const;
        /**
         * @brief Sends a command with the given \p position to the fingers motor of the hand.
         * @param position The new fingers position in motor ticks
         */
        void setFingersMotorPosition(int position) const;
        /**
         * @brief Sends a detailed command with the given \p position, \p maxVelocity and \p maxPWM
         * to the thumb motor of the hand.
         * @param position The new position in motor ticks
         * @param maxVelocity The maximal velocity in motor ticks per second
         * @param maxPWM The maximal PWM value of the motor
         */
        void setThumbMotorManualControl(int position, int maxVelocity, int maxPWM) const;
        /**
         * @brief Sends a detailed command with the given \p position, \p maxVelocity and \p maxPWM
         * to the fingers motor of the hand.
         * @param position The new position in motor ticks
         * @param maxVelocity The maximal velocity in motor ticks per second
         * @param maxPWM The maximal PWM value of the motor
         */
        void setFingersMotorManualControl(int position, int maxVelocity, int maxPWM) const;
        /**
         * @brief Sends a command to close the hand.
         */
        void closeHand() const;
        /**
         * @brief Sends a command to open the hand.
         */
        void openHand() const;
        /**
         * @brief Sends the given \p graspNumber as a predefiend grasp configuration to the hand.
         * @param graspNumber The number of the predefined grasp
         */
        void setPredefinedGraspConfiguration(unsigned int graspNumber) const;

    private:
        void connectionStateChangedCallback(const State c);
        void setupGuiAccordingToDevice(const HandDevice& d) const;

        void processReceivedValues() const;


        KITHandCommunicationDriverPtr _driver;
        std::vector<HandDevice> _availableDevices;

        std::atomic<bool> _scanThreadRunning {false};
        std::thread _scanThread;

        QBasicTimer _updateTimer;
    };
}
