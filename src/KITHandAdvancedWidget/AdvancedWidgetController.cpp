#include "AdvancedWidgetController.h"
#include <QCoreApplication>

namespace KITHand {
    AdvancedWidgetController::AdvancedWidgetController(QObject *parent) : QObject(parent)
    {
        _driver = std::make_shared<KITHandCommunicationDriver>();
        _driver->registerConnectionStateChangedCallback(std::bind(&AdvancedWidgetController::connectionStateChangedCallback, this, std::placeholders::_1));

        _scanThreadRunning = true;
        _scanThread = std::thread
        {
            [this]
            {
                scanForDevices();
            }
        };
    }

    AdvancedWidgetController::~AdvancedWidgetController()
    {
#ifdef KITHandWidget_WITH_ICE
        _server.stopIceServer();
#endif
        _scanThreadRunning = false;
        if (_scanThread.joinable())
        {
            _scanThread.join();
        }
        _driver->disconnect();
    }

    void AdvancedWidgetController::timerEvent(QTimerEvent *event)
    {
        if (event->timerId() == _updateTimer.timerId())
        {
            processReceivedValues();
        }
        else
        {
            QObject::timerEvent(event);
        }
    }

    void AdvancedWidgetController::scanForDevices()
    {
        while (_scanThreadRunning)
        {
            auto scanFuture = _driver->scanForDevicesAsync(KITHandCommunicationDriver::ScanMode::Both, 1000);
            scanFuture.wait();
            std::vector<HandDevice> devices = scanFuture.get();

            _availableDevices = devices;
            QStringList names;
            for (HandDevice& d : _availableDevices)
            {
                std::string name;
                if (!d.name.empty())
                {
                    name = d.name;
                }
                else if (!d.macAdress.empty())
                {
                    name = "MAC: " + d.macAdress;
                }
                else if (!d.serialDeviceFile.empty())
                {
                    name = "Port: " + d.serialDeviceFile;
                }
                names.push_back(QString::fromStdString(name + " (" + ((d.hardwareTarget == HardwareTarget::Bluetooth) ? "Bluetooth" : "Serial") + ")"));
            }
            emit scanFinished(names);
        }
    }

    void AdvancedWidgetController::connectToDeviceAtIndex(unsigned int index)
    {
        if (_availableDevices.size() > index)
        {
            HandDevice device = _availableDevices.at(index);
            if (_driver->connect(device))
            {
                setupGuiAccordingToDevice(device);

                _updateTimer.start(10, this);
                emit connectingFinished();
            }
            else
            {
                emit disconnectingFinished();
            }
        }
        else
        {
            emit disconnectingFinished();
        }
    }

    void AdvancedWidgetController::disconnectFromCurrentDevice()
    {
        _updateTimer.stop();

        emit updateConnectionStatus("Disconnecting ...");
        QCoreApplication::processEvents();
        _driver->disconnect();
        emit disconnectingFinished();
    }

    void AdvancedWidgetController::sendRaw(QString command) const
    {
        _driver->sendRaw(command.toStdString());
    }

    void AdvancedWidgetController::changeDeviceName(QString newName) const
    {
        if (newName.size() <= 32)
        {
            _driver->sendPseudoATCommand('N', newName.toStdString());
        }
    }

    void AdvancedWidgetController::setThumbMotorPosition(int position) const
    {
        _driver->sendThumbPosition(static_cast<std::uint64_t>(position));
    }

    void AdvancedWidgetController::setFingersMotorPosition(int position) const
    {
        _driver->sendFingersPosition(static_cast<std::uint64_t>(position));
    }

    void AdvancedWidgetController::setThumbMotorManualControl(int position, int maxVelocity, int maxPWM) const
    {
        _driver->sendThumbPosition(static_cast<std::uint64_t>(position), static_cast<std::uint64_t>(maxVelocity), static_cast<std::uint64_t>(maxPWM));
    }

    void AdvancedWidgetController::setFingersMotorManualControl(int position, int maxVelocity, int maxPWM) const
    {
        _driver->sendFingersPosition(static_cast<std::uint64_t>(position), static_cast<std::uint64_t>(maxVelocity), static_cast<std::uint64_t>(maxPWM));
    }

    void AdvancedWidgetController::closeHand() const
    {
        _driver->closeHand();
    }

    void AdvancedWidgetController::openHand() const
    {
        _driver->openHand();
    }

    void AdvancedWidgetController::setPredefinedGraspConfiguration(unsigned int graspNumber) const
    {
        if (graspNumber <= ControlOptions::maxGraspId)
        {
            _driver->sendGrasp(graspNumber);
        }
    }

    void AdvancedWidgetController::connectionStateChangedCallback(const State c)
    {
        QString stateString = QString(ConnectionState::ToString(c).data());
        if (ConnectionState::IsError(c) || c == State::Disconnected || c == State::Disconnecting)
        {
            emit connectingAllowed(true);
        }
        else
        {
            emit connectingAllowed(false);
        }

        if (c == State::DeviceLost)
        {
            _updateTimer.stop();
            emit disconnectingFinished();
        }

        emit updateConnectionStatus(stateString);
    }

    void AdvancedWidgetController::setupGuiAccordingToDevice(const HandDevice &d) const
    {
        emit setConnectedDeviceName(d.name.empty() ? "-- device name unkown --" : QString::fromStdString(d.name));
        emit setConnectionTypeName(QString::fromStdString(((d.hardwareTarget == HardwareTarget::Bluetooth) ? "Bluetooth (" + d.macAdress + ")" : "Serial (" + d.serialDeviceFile + ")")));
        SensorProtocolAbilities spa = GetProtocolProperties(d.abilities.sensorProtocol);
        emit togglePWMValuesAbility(spa.sendsPWM);
        emit toggleIMUValuesAbility(spa.sendsIMU);
        emit toggleDistanceValuesAbility(spa.sendsDistance);
        emit toggleAdditionalGraspsAbility(d.abilities.receivesAdditionalGraspCommands);
        emit toggleNameChangeAbility(d.abilities.bluetoothNameChangeable);
    }

    void AdvancedWidgetController::processReceivedValues() const
    {
        HandDevice* device_ptr = _driver->getCurrentConnectedDevice();
        if (device_ptr)
        {
            emit updatePos(_driver->getFingersPos(), _driver->getThumbPos());
            SensorProtocolAbilities spa = GetProtocolProperties(device_ptr->abilities.sensorProtocol);
            if (spa.sendsPWM)
            {
                emit updatePWM(_driver->getFingersPWM(), _driver->getThumbPWM());
            }
            if (spa.sendsIMU)
            {
                emit updateIMU(_driver->getIMURoll(), _driver->getIMUPitch(), _driver->getIMUYaw());
            }
            if (spa.sendsDistance)
            {
                emit updateDistance(_driver->getTimeOfFlight());
            }
        }
    }
}

