#include "KITHandAdvancedWidget.h"
#include "ui_KITHandAdvancedWidget.h"

#include <QInputDialog>
#include <QStyle>
#include <QDesktopWidget>
#include <QPainter>

namespace KITHand
{
    KITHandAdvancedWidget::KITHandAdvancedWidget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::KITHandAdvancedWidget)
    {
        ui->setupUi(this);

        toggleGuiElements(false);
        ui->advanced_widget->setVisible(false);
        ui->moreGrasps_widget->setVisible(false);
        this->adjustSize();

        // Add h2t-logo
        ui->h2tLogo_label->setPixmap(QPixmap(":/images/H2T_color.png"));

        // Add KIT-logo
        ui->kitLogo_label->setPixmap(QPixmap(":/images/KIT_color.png"));

        // Set image for closing hand
        ui->control_button_close->setPixmap(QPixmap(":/images/closed_front_medium"));
        // Set image for opening hand
        ui->control_button_open->setPixmap(QPixmap(":/images/open_medium"));

        setupVisualizationWidget();
        setupAdvancedManualControl();

        // Open the main widget in the center of the screen
        this->setGeometry(
                    QStyle::alignedRect(
                        Qt::LeftToRight,
                        Qt::AlignCenter,
                        this->size(),
                        qApp->desktop()->availableGeometry()
                    )
        );

        _controllerThread = new QThread(this);
        _controller = new AdvancedWidgetController(nullptr);
        _controller->moveToThread(_controllerThread);

        // scanning
        connect(_controller, SIGNAL(scanFinished(QStringList)), this, SLOT(updateScanResults(QStringList)));

        // connecting/disconnecting
        connect(ui->scan_button_connect, SIGNAL(released()), this, SLOT(connectToDevice()));
        connect(ui->scan_button_disconnect, SIGNAL(released()), this, SLOT(disconnectFromDevice()));
        connect(_controller, SIGNAL(connectingFinished()), this, SLOT(onConnectingFinished()));
        connect(_controller, SIGNAL(disconnectingFinished()), this, SLOT(onDisconnectingFinished()));
        connect(_controller, SIGNAL(connectingAllowed(bool)), this, SLOT(updateConnectPushButtons(bool)));
        connect(_controller, SIGNAL(updateConnectionStatus(QString)), this, SLOT(updateConnectionStateLabel(QString)), Qt::DirectConnection);
        connect(_controller, SIGNAL(setConnectedDeviceName(QString)), ui->connection_label_deviceName, SLOT(setText(QString)));
        connect(_controller, SIGNAL(setConnectionTypeName(QString)), ui->connection_label_type, SLOT(setText(QString)));

        // Advanced
        connect(ui->advanced_button_toggleAdvancedWidget, SIGNAL(released()), this, SLOT(toggleAdvancedWidget()));
        connect(ui->advanced_button_toggleMoreGraspsWidget, SIGNAL(released()), this, SLOT(toggleMoreGraspsWidget()));
        connect(ui->advanced_button_sendRaw, SIGNAL(released()), this, SLOT(sendRawButtonReleased()));

        // Setup of gui according to abilities of connected device
        connect(_controller, SIGNAL(togglePWMValuesAbility(bool)), this, SLOT(togglePWMValues(bool)));
        connect(_controller, SIGNAL(toggleIMUValuesAbility(bool)), this, SLOT(toggleIMUValues(bool)));
        connect(_controller, SIGNAL(toggleDistanceValuesAbility(bool)), this, SLOT(toggleDistanceValues(bool)));
        connect(_controller, SIGNAL(toggleAdditionalGraspsAbility(bool)), this, SLOT(toggleMoreGraspsButton(bool)));
        connect(_controller, SIGNAL(toggleNameChangeAbility(bool)), this, SLOT(toggleChangeNameButton(bool)));

        // Updating of received values
        connect(_controller, SIGNAL(updatePos(long, long)), this, SLOT(updatePos(long, long)));
        connect(_controller, SIGNAL(updatePWM(long, long)), this, SLOT(updatePWM(long, long)));
        connect(_controller, SIGNAL(updateIMU(long, long, long)), this, SLOT(updateIMU(long, long, long)));
        connect(_controller, SIGNAL(updateDistance(long)), this, SLOT(updateDistance(long)));

        // control
        QTimer* controlUpdateTimer = new QTimer(this);
        connect(controlUpdateTimer, SIGNAL(timeout()), this, SLOT(updateDials()));
        connect(ui->control_dial_thumb, SIGNAL(sliderMoved(int)), _controller, SLOT(setThumbMotorPosition(int)));
        connect(ui->control_dial_fingers, SIGNAL(sliderMoved(int)), _controller, SLOT(setFingersMotorPosition(int)));
        connect(ui->control_button_open, SIGNAL(released()), _controller, SLOT(openHand()));
        connect(ui->control_button_close, SIGNAL(released()), _controller, SLOT(closeHand()));
        connect(ui->moreGrasps_toolButton_g2, &QPushButton::released, _controller, [this]{_controller->setPredefinedGraspConfiguration(2);});
        connect(ui->moreGrasps_toolButton_g3, &QPushButton::released, _controller, [this]{_controller->setPredefinedGraspConfiguration(3);});
        connect(ui->moreGrasps_toolButton_g4, &QPushButton::released, _controller, [this]{_controller->setPredefinedGraspConfiguration(4);});
        connect(ui->moreGrasps_toolButton_g5, &QPushButton::released, _controller, [this]{_controller->setPredefinedGraspConfiguration(5);});
        connect(ui->moreGrasps_toolButton_g6, &QPushButton::released, _controller, [this]{_controller->setPredefinedGraspConfiguration(6);});
        connect(ui->moreGrasps_toolButton_g7, &QPushButton::released, _controller, [this]{_controller->setPredefinedGraspConfiguration(7);});
        connect(ui->advanced_manualControl_thumb_button_send, &QPushButton::released, _controller,
                [this]{_controller->setThumbMotorManualControl(ui->advanced_manualControl_thumb_spinBox_position->value(),
                                                               ui->advanced_manualControl_thumb_spinBox_velocity->value(),
                                                               ui->advanced_manualControl_thumb_spinBox_pwm->value());});
        connect(ui->advanced_manualControl_fingers_button_send, &QPushButton::released, _controller,
                [this]{_controller->setFingersMotorManualControl(ui->advanced_manualControl_fingers_spinBox_position->value(),
                                                               ui->advanced_manualControl_fingers_spinBox_velocity->value(),
                                                               ui->advanced_manualControl_fingers_spinBox_pwm->value());});

        // Other
        connect(ui->connection_button_changeName, SIGNAL(released()), this, SLOT(changeNameButtonReleased()));

        _controllerThread->start();
        controlUpdateTimer->start(100);
    }

    KITHandAdvancedWidget::~KITHandAdvancedWidget()
    {
        _controllerThread->quit();
        _controllerThread->wait();

        delete _controller;
#ifdef KITHandAdvancedWidget_WITH_3D_VISUALIZATION
        delete _viewerWidget;
#endif
        delete ui;
    }

    void KITHandAdvancedWidget::updateScanResults(QStringList names)
    {
        // Currently selected entry should be kept selected
        QString current = ui->scan_comboBox_result->currentText();

        ui->scan_comboBox_result->clear();
        ui->scan_comboBox_result->addItems(names);

        int index = names.indexOf(current);
        if (index != -1)
        {
            ui->scan_comboBox_result->setCurrentIndex(index);
        }
    }

    void KITHandAdvancedWidget::connectToDevice()
    {
        int index = ui->scan_comboBox_result->currentIndex();
        if (index != -1)
        {
            this->setEnabled(false);

            updateConnectPushButtons(false);
            QMetaObject::invokeMethod(_controller, "connectToDeviceAtIndex", Qt::QueuedConnection, Q_ARG(unsigned int, static_cast<unsigned int>(index)));
        }
    }

    void KITHandAdvancedWidget::disconnectFromDevice()
    {
        this->setEnabled(false);
        QMetaObject::invokeMethod(_controller, "disconnectFromCurrentDevice", Qt::QueuedConnection);
    }

    void KITHandAdvancedWidget::updateConnectPushButtons(bool connectingAllowed)
    {
        ui->scan_button_connect->setEnabled(connectingAllowed);
        ui->scan_button_disconnect->setDisabled(connectingAllowed);
    }

    void KITHandAdvancedWidget::updateConnectionStateLabel(QString stateString)
    {
        ui->connection_label_state->setText(stateString);
    }

    void KITHandAdvancedWidget::toggleAdvancedWidget()
    {
        if (ui->advanced_button_toggleAdvancedWidget->isChecked())
        {
            ui->advanced_widget->setVisible(true);
        }
        else
        {
            ui->advanced_widget->setVisible(false);
            if (!this->isMaximized())
            {
                this->adjustSize();
            }
        }
    }

    void KITHandAdvancedWidget::toggleMoreGraspsWidget()
    {

        if (this->isMaximized())
        {
            if (ui->advanced_button_toggleMoreGraspsWidget->isChecked())
            {
                ui->moreGrasps_widget->setVisible(true);
            }
            else
            {
                ui->moreGrasps_widget->setVisible(false);
            }
        }
        else
        {
            int marginSide = 0;
            ui->mainLayout->layout()->getContentsMargins(&marginSide, nullptr, nullptr, nullptr);
            int width = ui->moreGrasps_widget->width() + marginSide + ui->mainLayout->layout()->spacing();

            if (ui->advanced_button_toggleMoreGraspsWidget->isChecked() && !ui->moreGrasps_widget->isVisible())
            {
                // We need to adjust the position of the main widget since this widget opens to the left
                this->move(this->pos().x() - width, this->pos().y());
                ui->moreGrasps_widget->setVisible(true);
            }
            else if (!ui->advanced_button_toggleMoreGraspsWidget->isChecked() && ui->moreGrasps_widget->isVisible())
            {
                // We need to adjust the position of the main widget since this widget opens to the left
                ui->moreGrasps_widget->setVisible(false);
                this->adjustSize();
                this->move(this->pos().x() + width, this->pos().y());
            }
        }
    }

    void KITHandAdvancedWidget::toggleGuiElements(bool toggle)
    {
        // Enables-/Disables all Gui elements
        ui->advanced_button_toggleAdvancedWidget->setEnabled(toggle);
        ui->advanced_button_toggleMoreGraspsWidget->setEnabled(toggle);
        ui->control_button_open->setEnabled(toggle);
        ui->control_button_close->setEnabled(toggle);
        ui->control_dial_fingers->setEnabled(toggle);
        ui->control_dial_thumb->setEnabled(toggle);
        ui->connection_button_changeName->setEnabled(toggle);

        ui->advanced_button_sendRaw->setEnabled(toggle);
        ui->advanced_manualControl_fingers_button_send->setEnabled(toggle);
        ui->advanced_manualControl_fingers_spinBox_position->setEnabled(toggle);
        ui->advanced_manualControl_fingers_spinBox_pwm->setEnabled(toggle);
        ui->advanced_manualControl_fingers_spinBox_velocity->setEnabled(toggle);
        ui->advanced_manualControl_thumb_button_send->setEnabled(toggle);
        ui->advanced_manualControl_thumb_spinBox_position->setEnabled(toggle);
        ui->advanced_manualControl_thumb_spinBox_pwm->setEnabled(toggle);
        ui->advanced_manualControl_thumb_spinBox_velocity->setEnabled(toggle);

        ui->moreGrasps_toolButton_g2->setEnabled(toggle);
        ui->moreGrasps_toolButton_g3->setEnabled(toggle);
        ui->moreGrasps_toolButton_g4->setEnabled(toggle);
        ui->moreGrasps_toolButton_g5->setEnabled(toggle);
        ui->moreGrasps_toolButton_g6->setEnabled(toggle);
        ui->moreGrasps_toolButton_g7->setEnabled(toggle);
    }

    void KITHandAdvancedWidget::togglePWMValues(bool toggle)
    {
        ui->advanced_label_fingerPWM->setVisible(toggle);
        ui->advanced_passiveLabel_fingerPWM->setVisible(toggle);
        ui->advanced_label_thumbPWM->setVisible(toggle);
        ui->advanced_passiveLabel_thumbPWM->setVisible(toggle);
    }

    void KITHandAdvancedWidget::toggleIMUValues(bool toggle)
    {
        ui->advanced_groupBox_IMU->setVisible(toggle);
    }

    void KITHandAdvancedWidget::toggleDistanceValues(bool toggle)
    {
        ui->advanced_groupBox_DistanceSensor->setVisible(toggle);
    }

    void KITHandAdvancedWidget::toggleChangeNameButton(bool toggle)
    {
        ui->connection_stackedWidget_changeName->setCurrentIndex((toggle ? 0 : 1));
    }

    void KITHandAdvancedWidget::toggleMoreGraspsButton(bool toggle)
    {
        ui->advanced_button_toggleMoreGraspsWidget->setChecked(false);
        ui->advanced_button_toggleMoreGraspsWidget->setVisible(toggle);
    }

    void KITHandAdvancedWidget::sendRawButtonReleased()
    {
        QMetaObject::invokeMethod(_controller, "sendRaw", Qt::QueuedConnection, Q_ARG(QString, ui->advanced_lineEdit_sendRaw->text()));
    }

    void KITHandAdvancedWidget::changeNameButtonReleased()
    {
        bool ok = false;
        QString newName = QInputDialog::getText(this,
                                                tr("Change Name of connected Device"),
                                                tr("New Device-Name: (max. 15 Characters)"),
                                                QLineEdit::Normal,
                                                ui->connection_label_deviceName->text(),
                                                &ok);
        if (ok && !newName.isEmpty() && newName.length() <= 15) // max. 15 characters are allowed for the new name
        {
            QMetaObject::invokeMethod(_controller, "changeDeviceName", Qt::QueuedConnection, Q_ARG(QString, newName));
            ui->connection_label_deviceName->setText(newName);
        }
    }

    void KITHandAdvancedWidget::updatePos(long fingersPos, long thumbPos)
    {
        ui->advanced_label_fingerPos->setText(QString::number(fingersPos));
        ui->advanced_label_thumbPos->setText(QString::number(thumbPos));
#ifdef KITHandAdvancedWidget_WITH_3D_VISUALIZATION
        _viewerWidget->updateThumbPosition(thumbPos);
        _viewerWidget->updateFingerPosition(fingersPos);
#endif
    }

    void KITHandAdvancedWidget::updatePWM(long fingersPWM, long thumbPWM)
    {
        ui->advanced_label_fingerPWM->setText(QString::number(fingersPWM));
        ui->advanced_label_thumbPWM->setText(QString::number(thumbPWM));
    }

    void KITHandAdvancedWidget::updateIMU(long roll, long pitch, long yaw)
    {
        ui->advanced_label_IMURoll->setText(QString::number(roll));
        ui->advanced_label_IMUPitch->setText(QString::number(pitch));
        ui->advanced_label_IMUYaw->setText(QString::number(yaw));
    }

    void KITHandAdvancedWidget::updateDistance(long distance)
    {
        ui->advanced_label_distance->setText(QString::number(distance));
    }

    void KITHandAdvancedWidget::onConnectingFinished()
    {
        toggleGuiElements(true);
        this->setEnabled(true);
    }

    void KITHandAdvancedWidget::onDisconnectingFinished()
    {
        togglePWMValues(true);
        toggleIMUValues(true);
        toggleDistanceValues(true);
        toggleChangeNameButton(true);

        ui->advanced_button_toggleMoreGraspsWidget->setChecked(false);
        toggleMoreGraspsWidget();
        toggleMoreGraspsButton(false);

        ui->advanced_label_fingerPos->setText("-");
        ui->advanced_label_fingerPWM->setText("-");
        ui->advanced_label_thumbPos->setText("-");
        ui->advanced_label_thumbPWM->setText("-");
        ui->advanced_label_IMURoll->setText("-");
        ui->advanced_label_IMUPitch->setText("-");
        ui->advanced_label_IMUYaw->setText("-");
        ui->advanced_label_distance->setText("-");

        ui->connection_label_deviceName->setText("");
        ui->connection_label_type->setText("-");

        toggleGuiElements(false);
        this->setEnabled(true);
    }

    void KITHandAdvancedWidget::updateDials()
    {
        if (!ui->control_dial_thumb->isSliderDown() && ui->advanced_label_thumbPos->isEnabled())
        {
            int thumbPos = ui->advanced_label_thumbPos->text().toInt();
            if (thumbPos >= ui->control_dial_thumb->minimum() && thumbPos <= ui->control_dial_thumb->maximum())
            {
                ui->control_dial_thumb->setValue(thumbPos);
            }
        }

        if (!ui->control_dial_fingers->isSliderDown() && ui->advanced_label_fingerPos->isEnabled())
        {
            int fingerPos = ui->advanced_label_fingerPos->text().toInt();
            if (fingerPos >= ui->control_dial_fingers->minimum() && fingerPos <= ui->control_dial_fingers->maximum())
            {
                ui->control_dial_fingers->setValue(fingerPos);
            }
        }
    }

    void KITHandAdvancedWidget::setupVisualizationWidget()
    {
#ifdef KITHandAdvancedWidget_WITH_3D_VISUALIZATION
        _viewerWidget = new Hand3DViewerWidget(ui->visualization_widget);
        _viewerWidget->setMinimumSize(100, 100);
        _viewerWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        static_cast<QGridLayout*>(ui->visualization_widget->layout())->addWidget(_viewerWidget, 0, 0, 1, 2);
#else
        LOG_INFO << "3D-Visualization disabled";
#endif
    }

    void KITHandAdvancedWidget::setupAdvancedManualControl()
    {
        ui->advanced_manualControl_thumb_spinBox_position->setMinimum(0);
        ui->advanced_manualControl_thumb_spinBox_position->setMaximum(ControlOptions::maxPosThumb);
        ui->advanced_manualControl_thumb_spinBox_position->setValue(ControlOptions::maxPosThumb / 2);
        ui->advanced_manualControl_thumb_spinBox_velocity->setMinimum(ControlOptions::minVelocity);
        ui->advanced_manualControl_thumb_spinBox_velocity->setMaximum(ControlOptions::maxVelocity);
        ui->advanced_manualControl_thumb_spinBox_velocity->setValue(ControlOptions::maxVelocity);
        ui->advanced_manualControl_thumb_spinBox_pwm->setMinimum(0);
        ui->advanced_manualControl_thumb_spinBox_pwm->setMaximum(ControlOptions::maxPWM);
        ui->advanced_manualControl_thumb_spinBox_pwm->setValue(ControlOptions::maxPWM);

        ui->advanced_manualControl_fingers_spinBox_position->setMinimum(0);
        ui->advanced_manualControl_fingers_spinBox_position->setMaximum(ControlOptions::maxPosFingers);
        ui->advanced_manualControl_fingers_spinBox_position->setValue(ControlOptions::maxPosFingers / 2);
        ui->advanced_manualControl_fingers_spinBox_velocity->setMinimum(ControlOptions::minVelocity);
        ui->advanced_manualControl_fingers_spinBox_velocity->setMaximum(ControlOptions::maxVelocity);
        ui->advanced_manualControl_fingers_spinBox_velocity->setValue(ControlOptions::maxVelocity);
        ui->advanced_manualControl_fingers_spinBox_pwm->setMinimum(0);
        ui->advanced_manualControl_fingers_spinBox_pwm->setMaximum(ControlOptions::maxPWM);
        ui->advanced_manualControl_fingers_spinBox_pwm->setValue(ControlOptions::maxPWM);
    }
}
