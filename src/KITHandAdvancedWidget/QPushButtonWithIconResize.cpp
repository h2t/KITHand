#include "QPushButtonWithIconResize.h"
#include <QPainter>

void QPushButtonWithIconResize::paintEvent(QPaintEvent *aEvent)
{
    QPushButton::paintEvent(aEvent);
    _displayImage();
}

void QPushButtonWithIconResize::setPixmap(QPixmap aPicture)
{
    _qpSource = _qpCurrent = aPicture;
}

void QPushButtonWithIconResize::_displayImage()
{
    if (_qpSource.isNull()) //no image was set, don't draw anything
        return;

    float cw = width(), ch = height();
    float pw = _qpCurrent.width(), ph = _qpCurrent.height();

    if ((pw > cw && ph > ch && pw/cw > ph/ch) || //both width and high are bigger, ratio at high is bigger or
        (pw > cw && ph <= ch) || //only the width is bigger or
        (pw < cw && ph < ch && cw/pw < ch/ph) //both width and height is smaller, ratio at width is smaller
        )
        _qpCurrent = _qpSource.scaledToWidth(static_cast<int>(cw) - 10, Qt::TransformationMode::SmoothTransformation);
    else if ((pw > cw && ph > ch && pw/cw <= ph/ch) || //both width and high are bigger, ratio at width is bigger or
        (ph > ch && pw <= cw) || //only the height is bigger or
        (pw < cw && ph < ch && cw/pw > ch/ph) //both width and height is smaller, ratio at height is smaller
        )
        _qpCurrent = _qpSource.scaledToHeight(static_cast<int>(ch) - 10, Qt::TransformationMode::SmoothTransformation);

    int x = (static_cast<int>(cw) - _qpCurrent.width())/2, y = (static_cast<int>(ch) - _qpCurrent.height())/2;

    QPainter paint(this);
    paint.drawPixmap(x, y, _qpCurrent);
}
