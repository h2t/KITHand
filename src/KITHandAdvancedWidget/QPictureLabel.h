#pragma once
#include <QPixmap>
#include <QLabel>

/**
 * @brief This class represents a QLabel with a pixmap where the size of the pixmap is adapted to the size of the label.
 *
 * During each paint-cycle, the pixmap is fit into the QLabel without losing its ratio. If this QPictureLabel changes its
 * size during program execution, the pixmap is adapted accordingly.
 */
class QPictureLabel : public QLabel
{
private:
    QPixmap _qpSource; //preserve the original, so multiple resize events won't break the quality
    QPixmap _qpCurrent;

    void _displayImage();

public:
    /**
     * @brief Constructs a new QPictureLabel with the given widget as a parent
     * @param aParent The parent of this instance
     */
    QPictureLabel(QWidget *aParent) : QLabel(aParent) { }
    /**
     * @param aPicture The QPixmap for this instance
     */
    void setPixmap(QPixmap aPicture);

    /**
     * @brief Extends the paint event of the base QLabel to fit the pixmap into the current size of the QLabel without losing
     * the ratio of the pixmap.
     * @param aEvent The paint event description
     */
    void paintEvent(QPaintEvent *aEvent);
};
