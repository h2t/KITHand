#pragma once

#include "Hand3DViewer.h"
#include <QWidget>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

namespace KITHand {
    /**
     * @brief A QWidget holding a 3D representation of a hand.
     */
    class Hand3DViewerWidget : public QWidget
    {
        Q_OBJECT
    public:
        /**
         * @brief Constructs a new Hand3DViewerWidget with the given \p parent
         * @param parent The parent of this QWidget
         */
        explicit Hand3DViewerWidget(QWidget* parent = nullptr);
        ~Hand3DViewerWidget() override;
    public slots:
        /**
         * @brief Updates the position of the fingers with the given \p fingerPos.
         * @param fingerPos The new position in motor ticks.
         */
        void updateFingerPosition(long fingerPos);
        /**
         * @brief Updates the position of the thumb with the given \p thumbPos.
         * @param thumbPos The new position in motor ticks.
         */
        void updateThumbPosition(long thumbPos);
    private:
        void loadAndSetupHandVisualization();

        Hand3DViewer* _hand3DViewer;

        VirtualRobot::RobotPtr _hand;
        VirtualRobot::CoinVisualizationPtr _handVisu;

        VirtualRobot::RobotNodePtr thumbProx;
        VirtualRobot::RobotNodePtr thumbDist;

        VirtualRobot::RobotNodePtr indexProx;
        VirtualRobot::RobotNodePtr indexDist;
        VirtualRobot::RobotNodePtr middleProx;
        VirtualRobot::RobotNodePtr middleDist;
        VirtualRobot::RobotNodePtr ringProx;
        VirtualRobot::RobotNodePtr ringDist;
        VirtualRobot::RobotNodePtr pinkyProx;
        VirtualRobot::RobotNodePtr pinkyDist;
    };
}

