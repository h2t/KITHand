if (BUILD_ADVANCED_GUI)
    find_package(Qt5Core QUIET)
    find_package(Qt5Gui QUIET)
    find_package(Qt5Widgets QUIET)

    if(NOT Qt5Core_FOUND)
        message(STATUS "Failed to find Qt5Core! Not building GUI.")
    elseif(NOT Qt5Gui_FOUND)
        message(STATUS "Failed to find Qt5Gui! Not building GUI.")
    elseif(NOT Qt5Widgets_FOUND)
        message(STATUS "Failed to find Qt5Widgets! Not building GUI.")
    else()
        message(STATUS "Found all required parts of Qt5! Building KITHandAdvancedWidget.")
        set(CMAKE_AUTOMOC ON)
        set(CMAKE_AUTOUIC ON)
        set(CMAKE_INCLUDE_CURRENT_DIR ON)
        set(CMAKE_AUTORCC ON)

        add_library(
            KITHandAdvancedWidget
            SHARED
            KITHandAdvancedWidget.h
            KITHandAdvancedWidget.cpp
            KITHandAdvancedWidget.ui
            AdvancedWidgetController.h
            AdvancedWidgetController.cpp
            QPictureLabel.h
            QPictureLabel.cpp
            QPushButtonWithIconResize.h
            QPushButtonWithIconResize.cpp
            ../resources.qrc
        )

        target_link_libraries(
            KITHandAdvancedWidget
            PUBLIC
                Qt5::Widgets
                Qt5::Core
                Qt5::Gui

                KITHandCommunicationDriver
        )

        target_include_directories(
            KITHandAdvancedWidget
            PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
            $<INSTALL_INTERFACE:include/KITHand>
        )

        target_compile_definitions(
            KITHandAdvancedWidget
            PRIVATE
            KITHandAdvancedWidget_DATA_PATH="${CMAKE_CURRENT_SOURCE_DIR}/data"
        )

        # ------------------------------------------
        # Coin3D & SoQt & Simox
        # ------------------------------------------
        FIND_PACKAGE(Simox QUIET)
        if (Simox_FOUND)
            FIND_PACKAGE(Coin3D QUIET)
            if (COIN3D_FOUND)
                FIND_PACKAGE(SoQt QUIET)
                if (SOQT_FOUND)
                    target_sources(
                        KITHandAdvancedWidget
                        PRIVATE
                            Hand3DViewer.h
                            Hand3DViewer.cpp
                            Hand3DViewerWidget.h
                            Hand3DViewerWidget.cpp
                    )

                    target_link_libraries(
                        KITHandAdvancedWidget
                        PUBLIC
                            ${Coin3D_LIBRARIES}
                            ${SoQt_LIBRARIES}
                            VirtualRobot
                    )

                    target_include_directories(
                        KITHandAdvancedWidget
                        PRIVATE
                            ${SoQt_INCLUDE_DIRS}
                            ${COIN3D_INCLUDE_DIRS}
                    )

                    target_compile_definitions(
                        KITHandAdvancedWidget
                        PUBLIC -DKITHandAdvancedWidget_WITH_3D_VISUALIZATION
                    )

                else()
                    MESSAGE (STATUS "Did not find SoQt -->> building without 3DVisualization")
                endif()
            else ()
                MESSAGE (STATUS "Did not find Coin3D -->> building without 3DVisualization")
            endif()
        else()
            MESSAGE (STATUS "Did not find Simox -->> building without 3DVisualization")
        endif()


        if(TARGET KITHandIceServer)
            target_link_libraries(
               KITHandAdvancedWidget
               PUBLIC KITHandIceServer
            )
            target_compile_definitions(
                KITHandAdvancedWidget
                PUBLIC -DKITHandAdvancedWidget_WITH_ICE
            )
        endif()

        install(
            TARGETS KITHandAdvancedWidget
            EXPORT KITHandTargets
            LIBRARY DESTINATION lib
        )

        INSTALL(
            FILES KITHandAdvancedWidget.h
            DESTINATION include/KITHand
        )
    endif()
else()
    message(STATUS "NOT building: KITHandAdvancedWidget")
endif()
