#pragma once

#include <string>
#include <map>
#include <vector>
#include <regex>

namespace KITHand
{
    namespace MAC
    {
        static constexpr auto V1 = "DF:70:E8:81:DB:D6";
        static constexpr auto ARCHES_DLR = "DF:A2:F5:48:E7:50";
        static constexpr auto ARCHES_H2T = "E2:D4:89:F6:1E:1D";
        static constexpr auto V2_UMG = "CC:9E:3A:37:26:1A";
        static constexpr auto V2_LASA = "CB:43:34:8F:3C:0A";
        static constexpr auto H2T_WhiteRight = "C0:59:FC:0F:6B:B5"; // Aktuelle Hand mit Schaft und EKG-Sensoren
        static constexpr auto H2T_BlackLeft = "EE:40:A9:7E:86:D5"; // Kleine Schwarze Hand (links)
        static constexpr auto ARCHES_GEOMAR = "F0:1E:65:04:67:8F"; // Mein aktuelles Test-PCB
        static constexpr auto PCB3 = "DE:C3:B9:71:E8:28"; // Test-PCB von Pascal für Sensorized Fingers
    }

    /**
     * @brief Describes the whether the hand is a left hand or a right hand.
     */
    enum class HandOrientation
    {
        Left,
        Right,
        unknown
    };

    /**
     * @brief Describes the protocol with which the sensor values are transfered from the hand.
     */
    enum class SensorProtocol
    {
        TposTpwmFposFpwm,
        TposFposIMUDist,
        MxPosPwm,
        Binary,
        unknown
    };

    /**
     * @brief Describes the types of values that are transfered with a certain SensorProtocol
     */
    struct SensorProtocolAbilities
    {
        bool sendsPWM; /**< Whether a sensor protocol contains PWM values */
        bool sendsIMU; /**< Whether a sensor protocol contains IMU values */
        bool sendsDistance; /**< Whether a sensor protocol contains distance values */
    };

    /**
     * @brief Describes the protocol used to send commands to the hand.
     */
    enum class ControlProtocol
    {
        MX_Velocity_MaxPWM_Position,
        Ptf_Position_Velocity_ClosingPWM_tf,
        unknown
    };

    // This map maps the binary protocol version to the amount of bytes of data
    // that follow the version-byte
    static std::map<char, int> BinarySensorProtocolToDataLength =
    {
        {0x05, 38}
    };

    /**
     * @brief Returns the SensorProtocolAbilities of the given \p protocol
     * @param protocol The given SensorProtocol
     * @return the SensorProtocolAbilities of the given protocol
     */
    static SensorProtocolAbilities GetProtocolProperties(const SensorProtocol protocol)
    {
        switch(protocol)
        {
            case SensorProtocol::TposTpwmFposFpwm:
                return {true, false, false};
            case SensorProtocol::TposFposIMUDist:
                return {true, true, true};
            case SensorProtocol::MxPosPwm:
                return {true, false, false};
            case SensorProtocol::Binary:
                return {true, true, true};
            case SensorProtocol::unknown:
                return {false, false, false};
        }
        return {false, false, false};
    };

    /**
     * Contains definitions and global functions for parsing raw sensor data
     */
    namespace Parsing
    {
        #define end   R"([ \t\n\r\x00]*$)"
        #define start R"(^[ \t\x00]*)"
        #define white R"([ \t]+)"
        #define num   R"(([-+]?[1-9][0-9]*|0))"
        static std::vector<long long> MatchAndTryToConvertVaules(const std::string& data, const std::regex& regex)
        {
            std::vector<long long> result;
            std::smatch match;

            if (std::regex_match(data, match, regex))
            {
                for (size_t i = 1; i < match.size(); ++i)
                {
                    try {
                        result.push_back(std::stoll(match[i]));
                    } catch (std::invalid_argument e) {
                        result.clear();
                        return result;
                    }
                }
                return result;
            }
            else
            {
                return result;
            }
        }

        static std::vector<long long> ParseData_TposTpwmFposFpwm(const std::string& data)
        {
            static const std::regex regex {start num white num white num white num end};
            return MatchAndTryToConvertVaules(data, regex);
        }

        static std::vector<long long> ParseData_TposFposIMUDist(const std::string& data)
        {
            static const std::regex regex {start "PT:" white num "PF:" white num "R:" white num "P:" white num "Y:" white num "T:" white num end};
            return MatchAndTryToConvertVaules(data, regex);
        }

        static std::vector<long long> ParseData_MxPosPwm_M2(const std::string& data)
        {
            static const std::regex regex {start "M2:" white "Pos.:" white num white "PWM:" white num end};
            return MatchAndTryToConvertVaules(data, regex);
        }

        static std::vector<long long> ParseData_MxPosPwm_M3(const std::string& data)
        {
            static const std::regex regex {start "M3:" white "Pos.:" white num white "PWM:" white num end};
            return MatchAndTryToConvertVaules(data, regex);
        }
        #undef end
        #undef start
        #undef white
        #undef num
    }

    /**
     * @brief Describes whether a HandDevice will be (or currently is) connected via Bluetooth LE oder a serial port.
     */
    enum class HardwareTarget
    {
        Bluetooth,
        Serial,
        unknown
    };

    /**
     * @brief Describes the abilities of a hand.
     */
    struct HandAbilities
    {
        bool bluetoothNameChangeable; /**< Whether the firmware on the hand supports changing the name of the bluetooth device */
        bool receivesAdditionalGraspCommands; /**< Whether the firmware on the hand supports additional grasp commands besides 'g0' and 'g1' */
        SensorProtocol sensorProtocol; /**< Protocol version of the data that is received from the hand */
        ControlProtocol controlProtocol; /**< Protocol version of the data that is sent to the hand */
        HandOrientation handOrientation; /**< HandOrientation */
        std::string handSize; /**< Size of the hand */
    };

    /**
     * @brief Describes a hand with all its abilities and how it is connected to the PC.
     */
    struct HandDevice
    {
        std::string name; /**< Name of the hand */
        std::string macAdress; /**< Mac-Address of the bluetooth module on the hand, only relevant if HandDevice::hardwareTarget is Bluetooth */
        HandAbilities abilities; /**< Abilities of the hand */
        HardwareTarget hardwareTarget; /**< Either Bluetooth or Serial, depending on current connection type */
        std::string serialDeviceFile; /**< Name of the file that represents the serial port the hand is connected to,
                                            only relevant if HandDevice::hardwareTarget is Serial */
    };

    /**
     * @brief Compares two given HandDevice and returns true if they are equal
     * @param d1 The first HandDevice
     * @param d2 The second HandDevice
     * @return true if \p d1 is equal to \p d2
     */
    inline bool operator== (HandDevice& d1, HandDevice& d2)
    {
        return  d1.name == d2.name &&
                d1.macAdress == d2.macAdress &&
                d1.hardwareTarget == d1.hardwareTarget &&
                d1.serialDeviceFile == d2.serialDeviceFile &&
                d1.abilities.bluetoothNameChangeable == d2.abilities.bluetoothNameChangeable &&
                d1.abilities.handOrientation == d2.abilities.handOrientation &&
                d1.abilities.handSize == d2.abilities.handSize &&
                d1.abilities.sensorProtocol == d2.abilities.sensorProtocol &&
                d1.abilities.controlProtocol == d2.abilities.controlProtocol &&
                d1.abilities.receivesAdditionalGraspCommands == d2.abilities.receivesAdditionalGraspCommands;
    }


    static const HandDevice HandInfo[9] = {
        {
            "V1",
             MAC::V1,
             { // Default values
                false,
                false,
                SensorProtocol::MxPosPwm,
                ControlProtocol::MX_Velocity_MaxPWM_Position,
                HandOrientation::Right,
                "unknown"
             },
             HardwareTarget::unknown,
             ""
        },
        {
            "ARCHES_DLR",
            MAC::ARCHES_DLR,
            {
                false,
                true,
                SensorProtocol::MxPosPwm,
                ControlProtocol::MX_Velocity_MaxPWM_Position,
                HandOrientation::Right,
                "large"
            },
            HardwareTarget::Bluetooth,
            ""
        },
        {
            "ARCHES_H2T",
            MAC::ARCHES_H2T,
            {
                false,
                true,
                SensorProtocol::MxPosPwm,
                ControlProtocol::MX_Velocity_MaxPWM_Position,
                HandOrientation::Right,
                "large"
            },
            HardwareTarget::Bluetooth,
            ""
        },
        {
            "V2_UMG",
            MAC::V2_UMG,
            {
                false,
                false,
                SensorProtocol::TposFposIMUDist,
                ControlProtocol::Ptf_Position_Velocity_ClosingPWM_tf,
                HandOrientation::Right,
                "medium"
            },
            HardwareTarget::unknown,
            ""
        },
        {
            "V2_LASA",
            MAC::V2_LASA,
            {
                false,
                false,
                SensorProtocol::TposFposIMUDist,
                ControlProtocol::Ptf_Position_Velocity_ClosingPWM_tf,
                HandOrientation::Right,
                "medium"
            },
            HardwareTarget::unknown,
            ""
        },
        {
            "H2T_MediumRight",
            MAC::H2T_WhiteRight,
            {
                false, // true
                false,
                SensorProtocol::Binary,
                ControlProtocol::Ptf_Position_Velocity_ClosingPWM_tf,
                HandOrientation::Right,
                "medium"
            },
            HardwareTarget::unknown,
            ""
        },
        {
            "H2T_SmallLeft",
            MAC::H2T_BlackLeft,
            {
                true,
                false,
                SensorProtocol::Binary, // SensorProtocol::Binary
                ControlProtocol::Ptf_Position_Velocity_ClosingPWM_tf,
                HandOrientation::Left,
                "small"
            },
            HardwareTarget::unknown,
            ""
        },
        {
            "ARCHES_GEOMAR",
            MAC::ARCHES_GEOMAR,
            {
                true,
                true,
                SensorProtocol::MxPosPwm,
                ControlProtocol::MX_Velocity_MaxPWM_Position,
                HandOrientation::Right,
                "large"
            },
            HardwareTarget::unknown,
            ""
        },
        {
            "PCB3",
            MAC::PCB3,
            {
                true,
                false,
                SensorProtocol::Binary,
                ControlProtocol::Ptf_Position_Velocity_ClosingPWM_tf,
                HandOrientation::unknown,
                "unknown"
            },
            HardwareTarget::unknown,
            ""
        },
    };
}
