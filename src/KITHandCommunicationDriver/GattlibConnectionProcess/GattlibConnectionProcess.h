#pragma once

#include <bluetooth/sdp.h>
#include "../SharedMemory.h"
#include "../KITHandCommunicationDriverBase.h"

struct _gatt_connection_t;

namespace KITHand
{
    /**
     * @brief Is used to establish a connection
     * to a hand vie Bluetooth LE using the gatlib library.
     *
     * This class should only be used from another process and not run standalone, because it requires a set of SharedMemory instances
     * to work.
     */
    class GattlibConnectionProcess
    {
    public:
        /**
         * @brief Try to connect to a Bluetooth LE device with the given \p targetAddr and sends and receives data with SharedMemory
         *
         * This constructor performs following steps:
         * 1. Connect to SharedMemory instances that have been created by the parent process.
         * 2. Try to connect to the device specified by \p targetAddr
         * 3. Discover services of the device
         * 4. Register notificationCallback and disconnectionCallback in gattlib
         * 5. Start readWriteLoop, where any received data from the device is written to a SharedMemory and every data available in
         * another SharedMemory is send to the device
         *
         * @param targetAddr The mac-address of the device to connect to
         */
        GattlibConnectionProcess(const std::string& targetAddr);
        ~GattlibConnectionProcess();

    private:
        void _disconnect();
        bool doConnectDevice();
        bool doDiscoverService();
        void readWriteLoop();
        void setConnectionState(State state);

        //friends
        friend void notificationCallback(const uuid_t*, const uint8_t*, size_t, void*);
        friend void disconnectionCallback(void*);

        void*                       _gattAdapter = nullptr;
        _gatt_connection_t*         _gattConnection = nullptr;
        bool                        _gattCharWriteFound = false;
        bool                        _gattCharReadFound = false;
        bool                        _gattCharReadNotificationActive = false;
        uuid_t                      _gattCharwrite;
        uuid_t                      _gattCharRead;

        std::string                 _targAdr;

        SharedMemoryPtr             _sharedMemoryBluetoothReceive;
        SharedMemoryPtr             _sharedMemoryBluetoothSend;
        SharedMemoryPtr             _sharedMemoryConnectionStateSend;
        SharedMemoryPtr             _sharedMemoryKillProcess;
        std::atomic_bool            _connectionActive {false};
    };
}
