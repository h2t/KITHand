#include "GattlibConnectionProcess.h"
#include <gattlib.h>
#include <glib.h>

#include <iomanip>

namespace KITHand
{
    static const std::string uuid_uart_service = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
    static const std::string uuid_write_characteristic = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
    static const std::string uuid_read_characteristic = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";

    void notificationCallback(
        const uuid_t*,
        const uint8_t*  data,
        size_t          data_length,
        void*           connection_ptr
    )
    {
        auto obj = reinterpret_cast<GattlibConnectionProcess*>(connection_ptr);
        const char* cdata = reinterpret_cast<const char*>(data);
        std::vector<char> v;
        for (size_t i = 0; i < data_length; ++i)
        {
            v.push_back(cdata[i]);
        }
        obj->_sharedMemoryBluetoothReceive->push(std::move(v));
    }

    void disconnectionCallback(void* connection_ptr)
    {
        auto obj = reinterpret_cast<GattlibConnectionProcess*>(connection_ptr);
        LOG_INFO << "DISCONNECT DETECTED";
        obj->setConnectionState(State::DeviceLost);

        obj->_connectionActive = false;
    }

    GattlibConnectionProcess::GattlibConnectionProcess(const std::string &targetAddr) : _targAdr(targetAddr)
    {
        // Initializing SharedMemory
        try
        {
            _sharedMemoryBluetoothReceive = std::make_unique<SharedMemory>(SHARED_MEMORY_BLUETOOTH_RECEIVE_NAME, SharedMemory::WRITE);
            _sharedMemoryBluetoothSend = std::make_unique<SharedMemory>(SHARED_MEMORY_BLUETOOTH_SEND_NAME, SharedMemory::READ);
            _sharedMemoryConnectionStateSend = std::make_unique<SharedMemory>("KITHand_ConnectionState", SharedMemory::WRITE);
            _sharedMemoryKillProcess = std::make_unique<SharedMemory>("KITHand_kill", SharedMemory::READ);
        }
        catch (const boost::interprocess::interprocess_exception& e)
        {
            LOG_FATAL << "Interprocess-Exception: " << e.what() << std::endl;
            return;
        }
        setConnectionState(State::TryingToConnect);

        LOG_INFO << "Connecting to device";
        if (!doConnectDevice())
        {
            setConnectionState(State::FailedToConnectDevice);
            return;
        }
        LOG_INFO << "Discovering services";
        if(!doDiscoverService())
        {
            _disconnect();

            if(!_gattCharReadFound && !_gattCharWriteFound)
            {
                setConnectionState(State::FailedToFindReadWriteCharacteristics);
                return;
            }
            if(!_gattCharReadFound)
            {
                setConnectionState(State::FailedToFindReadCharacteristic);
                return;
            }
            if(!_gattCharWriteFound)
            {
                setConnectionState(State::FailedToFindWriteCharacteristic);
                return;
            }
        }

        LOG_INFO << "Register notifications and callbacks";
        gattlib_register_notification(_gattConnection, notificationCallback, this);
        gattlib_register_on_disconnect(_gattConnection, disconnectionCallback,this);
        _gattCharReadNotificationActive = !gattlib_notification_start(_gattConnection, &_gattCharRead);

        if(!_gattCharReadNotificationActive)
        {
            _disconnect();
            LOG_WARN << "failed to start notifications for the read characteristic!"
                   << "No sensor values will be available";
            setConnectionState(State::ConnectedButNoSensorValues);
            return;
        }

        LOG_INFO << "Connected.";
        setConnectionState(State::Connected);
        // Enter write loop
        _connectionActive = true;
        readWriteLoop();
    }

    GattlibConnectionProcess::~GattlibConnectionProcess()
    {
        if (_connectionActive)
        {
            _connectionActive = false;
            _disconnect();
            setConnectionState(State::Disconnected);
        }
    }

    void GattlibConnectionProcess::_disconnect()
    {
        if(_gattCharReadNotificationActive)
        {
            LOG_INFO << "Stopping gattlib notification";
            gattlib_notification_stop(_gattConnection, &_gattCharRead);
            _gattCharReadNotificationActive = false;
        }
        if(_gattConnection)
        {
            LOG_INFO << "Disconnecting gattlib";
            gattlib_disconnect(_gattConnection);
            _gattConnection = nullptr;
        }
        if(_gattAdapter)
        {
            LOG_INFO << "Closing gattlib adapter";
            gattlib_adapter_close(_gattAdapter);
            _gattAdapter = nullptr;
        }
    }

    bool GattlibConnectionProcess::doConnectDevice()
    {
        const char* addr = _targAdr.c_str();
        LOG_INFO << "connecting to bluetooth device with macAddress '" << _targAdr << "'";
        _gattConnection = gattlib_connect(nullptr, addr, GATTLIB_CONNECTION_OPTIONS_LEGACY_DEFAULT);
        if (_gattConnection)
        {
            LOG_INFO << "Succeeded to connect to the bluetooth device.";
            return true;
        }
        LOG_WARN << "Failed to connect to the bluetooth device.";
        return false;
    }

    bool GattlibConnectionProcess::doDiscoverService()
    {
        _gattCharWriteFound = false;
        _gattCharReadFound = false;
        char uuid_str[MAX_LEN_UUID_STR + 1];

    #define ohex(...)    std::hex << std::setw(8) << std::setfill('0') << __VA_ARGS__ <<  std::dec << std::setw(0) << std::setfill(' ')
    #define odec(i, ...) std::dec << std::setw(i) << std::setfill('0') << __VA_ARGS__ <<  std::dec << std::setw(0) << std::setfill(' ')
        //characteristics
        {
            gattlib_characteristic_t* characteristics;
            int characteristics_count;
            if (gattlib_discover_char(_gattConnection, &characteristics, &characteristics_count))
            {
                LOG_WARN << "Fail to discover characteristics.";
                return false;
            }

            LOG_INFO << "searching r/w characteristics";
            for (int i = 0; i < characteristics_count; i++)
            {
                gattlib_uuid_to_string(&characteristics[i].uuid, uuid_str, sizeof(uuid_str));

                LOG_INFO << "    characteristic[" << odec(3,i)
                       << "] properties:0x" << ohex(static_cast<unsigned>(characteristics[i].properties))
                       << " value_handle:0x" << ohex(characteristics[i].value_handle)
                       << " uuid:" << uuid_str;
                if(uuid_str == uuid_read_characteristic)
                {
                    LOG_INFO << "        this is the read characteristic!";
                    _gattCharReadFound = true;
                    _gattCharRead = characteristics[i].uuid;
                }
                if(uuid_str == uuid_write_characteristic)
                {
                    LOG_INFO << "        this is the write characteristic!";
                    _gattCharWriteFound = true;
                    _gattCharwrite = characteristics[i].uuid;
                }
                if(_gattCharReadFound && _gattCharWriteFound)
                {
                    break;
                }

            }
            free(characteristics);
            if(!_gattCharReadFound && !_gattCharWriteFound)
            {
                LOG_WARN << "read and write characteristic '" << uuid_read_characteristic << "' not found";
                return false;
            }
            if(!_gattCharReadFound)
            {
                LOG_WARN << "read characteristic '" << uuid_read_characteristic << "' not found";
                return false;
            }
            if(!_gattCharWriteFound)
            {
                LOG_WARN << "write characteristic '" << uuid_write_characteristic << "' not found";
                return false;
            }
        }
        return true;
    #undef ohex
    #undef odec
    }

    void GattlibConnectionProcess::setConnectionState(State state)
    {
        _sharedMemoryConnectionStateSend->push(std::vector<char> {static_cast<char>(state)});
    }

    void GattlibConnectionProcess::readWriteLoop()
    {
        if(!_gattCharWriteFound || !_gattCharReadFound)
        {
            LOG_WARN << "Either write or read characteristic were not found!";
            return;
        }

        std::string send_buffer;
        std::int64_t t_read = 0;
        std::int64_t t_write = 0;
        std::int64_t t_sleep = 0;
        std::uint64_t n_iterations = 0;

        LOG_INFO << "starting the read/write loop";
        const auto lbeg = std::chrono::high_resolution_clock::now();
        while(_connectionActive)
        {
            const auto ibeg = std::chrono::high_resolution_clock::now();
            ++n_iterations;
            //handle read notifications
            {
                const auto rbeg = std::chrono::high_resolution_clock::now();
                g_main_context_iteration(nullptr, false);
                const auto rend = std::chrono::high_resolution_clock::now();
                t_read += std::chrono::duration_cast<std::chrono::nanoseconds>(rend-rbeg).count();
            }
            //write
            {
                if (_sharedMemoryBluetoothSend->dataAvailable())
                {
                    std::vector<char> sendData = _sharedMemoryBluetoothSend->pop();
                    char* sendBuffer = reinterpret_cast<char*>(sendData.data());
                    const auto wbeg = std::chrono::high_resolution_clock::now();
                    gattlib_write_char_by_uuid(_gattConnection, &_gattCharwrite, sendBuffer, sendData.size());
                    const auto wend = std::chrono::high_resolution_clock::now();
                    t_write += std::chrono::duration_cast<std::chrono::nanoseconds>(wend-wbeg).count();
                }
            }
           //sleep
            {
                const auto sbeg = std::chrono::high_resolution_clock::now();
                std::this_thread::sleep_until(ibeg + std::chrono::milliseconds(1));
                const auto send = std::chrono::high_resolution_clock::now();
                t_sleep += std::chrono::duration_cast<std::chrono::nanoseconds>(send-sbeg).count();
            }
            // check for kill signal
            {
                if (_sharedMemoryKillProcess->dataAvailable())
                {
                    if (std::vector<char> v =_sharedMemoryKillProcess->pop(); std::string(v.begin(),v.end()) == "kill")
                    {
                        _connectionActive = false;
                        setConnectionState(State::Disconnecting);
                        _disconnect();
                    }
                }
            }

        }
        const auto lend = std::chrono::high_resolution_clock::now();
        const std::int64_t t_loop = std::chrono::duration_cast<std::chrono::nanoseconds>(lend-lbeg).count();
        const std::int64_t t_other = t_loop - t_read - t_write - t_sleep;

        const double t_loop_ms  = t_loop  / 1000000.0;
        const double t_read_ms  = t_read  / 1000000.0;
        const double t_write_ms = t_write / 1000000.0;
        const double t_sleep_ms = t_sleep / 1000000.0;
        const double t_other_ms = t_other / 1000000.0;

        LOG_INFO << "exiting the read/write loop";
        LOG_INFO << "time r/w loop        = " << t_loop_ms  << " ms (" << t_loop_ms  / n_iterations << " ms/iter)";
        LOG_INFO << "time gattlib_read    = " << t_read_ms  << " ms (" << t_read_ms  / n_iterations << " ms/iter)\t-> " << 100.0 * t_read  / t_loop << "%";
        LOG_INFO << "time gattlib_write   = " << t_write_ms << " ms (" << t_write_ms / n_iterations << " ms/iter)\t-> " << 100.0 * t_write / t_loop << "%";
        LOG_INFO << "time sleep           = " << t_sleep_ms << " ms (" << t_sleep_ms / n_iterations << " ms/iter)\t-> " << 100.0 * t_sleep / t_loop << "%";
        LOG_INFO << "time other           = " << t_other_ms << " ms (" << t_other_ms / n_iterations << " ms/iter)\t-> " << 100.0 * t_other / t_loop << "%";
        LOG_INFO << "number of iterations = " << n_iterations;
    }
}
