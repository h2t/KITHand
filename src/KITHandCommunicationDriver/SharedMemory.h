#pragma once

#include <vector>

#include <boost/next_prior.hpp>
#include <boost/lockfree/spsc_queue.hpp> // ring buffer
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/vector.hpp>

namespace KITHand
{
    #define SHARED_MEMORY_BLUETOOTH_SEND_NAME "KITHand_Bluetooth_Send"
    #define SHARED_MEMORY_BLUETOOTH_RECEIVE_NAME "KITHand_Bluetooth_Receive"

    namespace shm
    {
        typedef boost::interprocess::allocator<char, boost::interprocess::managed_shared_memory::segment_manager> char_alloc;
        typedef boost::interprocess::vector<char, char_alloc> sharedVector;
        typedef boost::interprocess::allocator<sharedVector, boost::interprocess::managed_shared_memory::segment_manager> vector_alloc;
        typedef boost::lockfree::spsc_queue<sharedVector, boost::lockfree::capacity<200>> RingBuffer;
    }

    /**
     * @brief Offers a queue-like datastructure to transfer data between two processes.
     *
     * Each process has to instantiate an instance of a SharedMemory with the same name.
     * It is important that only exactly ONE of both processes actually creates the shared memory with the flag in the constructor. <br>
     * One instance can always only be used for either writing or receiving data. <br>
     * It is possible to instantiate more than two instances of SharedMemory, but this might result in convoluted queue management and
     * is not recommended.
     */
    class SharedMemory
    {
    public:
        /**
         * @brief The Mode enum represents the mode of a SharedMemory instance
         */
        enum Mode
        {
            READ,
            WRITE
        };

        SharedMemory() = delete;
        /**
         * @brief Either creates a new shared memory segment with the given name or connects to an existing SharedMemory.
         *
         * The name of a shared memory segment has to be unique, because it functions as its identifier. <br>
         * A shared memory segment can only be created once. An attempt to create a new SharedMemory with the
         * same name as an existing one will result in an error.
         *
         * @param name The unique name of the shared memory segment
         * @param mode The mode of this instance
         * @param create If set to true, a new shared memory segment will be created
         */
        SharedMemory(const std::string& name, Mode mode, bool create = false);
        ~SharedMemory();

        /**
         * @brief Removes the oldest data entry in the queue and returns a copy of it.
         *
         * If the queue is empty, an empty vector is returned. If this function is called on a SharedMemory with the Mode WRITE, nothing
         * will be returned even if the queue is not empty.
         * @return the oldest data entry in the queue
         */
        std::vector<char> pop();
        /**
         * @brief Pushes a new data entry onto the queue.
         *
         * If the queue is full, the oldest entry is overwritten. Data can only be added to the SharedMemory if its Mode is WRITE.
         * @param data The data to push onto the queue
         */
        void push(const std::vector<char> data);

        /**
         * @return true if data is available
         */
        bool dataAvailable() const;

        /**
         * @brief Cleans up the shared memory segment.
         *
         * This function has to be called atleast once to prevent memory leakage and a segfault when program is terminated.
         * But it is enough if one of the two involved processes cleans it up.
         */
        void cleanup();

    private:
        boost::interprocess::managed_shared_memory _sharedMemory;
        shm::RingBuffer* _queue;
        std::string _name;
        Mode _mode;
    };

    /**
     * @brief UniquePtr to a SharedMemory
     */
    typedef std::unique_ptr<SharedMemory> SharedMemoryPtr;
}
