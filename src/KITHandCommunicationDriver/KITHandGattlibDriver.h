#pragma once

#include "KITHandCommunicationDriverBase.h"
#include "SharedMemory.h"

#include <bluetooth/sdp.h>
#include <atomic>
#include <boost/process/child.hpp>

namespace KITHand
{
    /**
     * @brief Implementation of KITHandCommunicationDriverBase for communication over Bluetooth LE
     *
     * The control of the bluetooth-device of the PC is realised by using the library <a href="https://github.com/labapart/gattlib">gattlib</a>
     */
    class KITHandGattlibDriver : public KITHandCommunicationDriverBase
    {
    public:
        /**
         * @brief Constructs a new KITHandGattlibDriver if a bluetooth device is available on this system.
         * @throws std::runtime_error if no bluetooth device is available
         */
        KITHandGattlibDriver();
        ~KITHandGattlibDriver() override;
        /**
         * @brief Tries to conenct ot the given \p device.
         *
         * Uses a child process to connect via gattlib because gattlib does not support reconnecting within the same process.
         * Blocking call until either the connection failed or it has been sucessfully established.
         * Until then this constructor performs following steps:
         * 1. Creating a set of SharedMemory instances for the communication with the child process
         * 2. Starting child process
         * 3. Waiting until connection state has settled
         *
         * @param device The HandDevice representing the hand to conenct to
         */
        void connect(HandDevice &device) override;
        /**
         * @brief Disconnect from the current hand.
         *
         * Signals the child process to disconnect from the currently connected device and to terminate itself.
         */
        void disconnect() override;
        std::future<std::vector<HandDevice>> scanForDevices(unsigned int timeoutMS = 2000) override;
        void registerDataAvaiableCallback(std::function<void (const std::vector<char>&)> callback) override;
        void registerConnectionStateChangedCallback(std::function<void(const State)> callback) override;
        void sendRaw(const std::vector<char> raw) override;
        std::chrono::milliseconds getMinimalTimeBetweenSendCommands() override;

    private:
        void setConnectionState(const State state);
        bool isBluetoothAvailable() const;
        void _disconnect();
        bool doConnectDevice();
        bool doDiscoverService();

        friend void notificationCallback(const uuid_t*, const uint8_t*, size_t, void*);
        friend void disconnectionCallback(void*);

        std::vector<HandDevice> scanWorker(unsigned int timeoutMS);
        bool                        _scanningAllowed {true};
        bool                        _scanActive {false};

        std::function<void (const std::vector<char>&)> _dataCallback;
        std::function<void (const State)> _connectionChangedCallback;

        State _currentConnectionState;
        std::chrono::time_point<std::chrono::high_resolution_clock> _timestampLastSendCall = std::chrono::high_resolution_clock::now();

        boost::process::child _gattlibConnectionProcess;
        SharedMemoryPtr _sharedMemoryBluetoothReceive;
        SharedMemoryPtr _sharedMemoryBluetoothSend;
        SharedMemoryPtr _sharedMemoryConnectionState;
        SharedMemoryPtr _sharedMemoryStopProcess;

        std::atomic_bool _receiveThreadStop {false};
        std::thread _receiveThread;
        void receiveLoop();
    };

    /**
     * @brief UniquePtr to a KITHandGattlibDriver
     */
    typedef std::unique_ptr<KITHandGattlibDriver> KITHandGattlibDriverPtr;
}
