#include <gattlib.h>
#include <glib.h>

#include "KITHandGattlibDriver.h"

#include <boost/process/async.hpp>
#include <boost/process/io.hpp>
#include <boost/exception_ptr.hpp>

using namespace boost::interprocess;

#define MAX_SEND_FREQUENCY 10 // in Hz. Any data that is tried to be sent faster will be skipped

namespace KITHand
{
    KITHandGattlibDriver::KITHandGattlibDriver()
    {
        // check if bluetooth is available on the system on throw exception if not
        if (!isBluetoothAvailable())
        {
            throw std::runtime_error("Bluetooth not available on this system. Cannot initialize KITHandGattlibDriver.");
        }
    }

    KITHandGattlibDriver::~KITHandGattlibDriver()
    {
        disconnect();
    }

    void KITHandGattlibDriver::connect(HandDevice &device)
    {
        // Clean up any potential old thread (this thread is still active in case the driver is disconnected due
        // to an connection state that represents an error
        if (_receiveThread.joinable())
        {
            _receiveThreadStop = true;
            _receiveThread.join();
        }

        LOG_INFO << "connecting ...";

        // Wait until current scanning has finished
        _scanningAllowed = false;
        while (_scanActive) { std::this_thread::sleep_for(std::chrono::milliseconds(1)); }

        // Initializing shared memory
        try {
            _sharedMemoryBluetoothSend = std::make_unique<SharedMemory>(SHARED_MEMORY_BLUETOOTH_SEND_NAME, SharedMemory::WRITE, true);
            _sharedMemoryBluetoothReceive = std::make_unique<SharedMemory>(SHARED_MEMORY_BLUETOOTH_RECEIVE_NAME, SharedMemory::READ, true);
            _sharedMemoryConnectionState = std::make_unique<SharedMemory>("KITHand_ConnectionState", SharedMemory::READ, true);
            _sharedMemoryStopProcess = std::make_unique<SharedMemory>("KITHand_kill", SharedMemory::WRITE, true);
        } catch (const boost::interprocess::interprocess_exception& e) {
            LOG_FATAL << "Interprocess-Exception: " << e.what() << std::endl;
            setConnectionState(State::FailedToConnectDevice);
            return;
        }

        // Start read thread for data and connectionState
        _receiveThreadStop = false;
        _receiveThread = std::thread
        {
            [this]
            {
                receiveLoop();
            }
        };

        // Create connection process
        namespace bp = boost::process;
        try {
            _gattlibConnectionProcess = bp::child(std::string(BINARY_DIR) + "/GattlibConnectionProcess",
                                                  device.macAdress);
            _gattlibConnectionProcess.detach(); // Detaching from child to continue non-blocking
        } catch (boost::exception& e) {
            LOG_WARN << boost::diagnostic_information(e);
            setConnectionState(State::FailedToConnectDevice);
            return;
        }

        setConnectionState(State::TryingToConnect);

        // Wait until connection settled
        while (_currentConnectionState == State::TryingToConnect)
        {
            LOG_INFO << "Trying to connect ...";
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        _scanningAllowed = true;
    }

    void KITHandGattlibDriver::disconnect()
    {
        if (_receiveThread.joinable())
        {
            _receiveThreadStop = true;
            _receiveThread.join();
        }

        if (_gattlibConnectionProcess.valid() && _gattlibConnectionProcess.running())
        {
            _sharedMemoryStopProcess->push(std::vector<char>({'k', 'i', 'l', 'l'}));
            _gattlibConnectionProcess.wait();
            setConnectionState(State::Disconnected);
        }
        _disconnect();
    }

    std::vector<HandDevice> KITHandGattlibDriver::scanWorker(unsigned int timeoutMS)
    {
        std::vector<HandDevice> devices = std::vector<HandDevice>();
        if (!_scanActive && _scanningAllowed)
        {
            _scanActive = true;
            try {
                boost::process::child gattlibScanProcess(std::string(BINARY_DIR) + "/GattlibScanProcess",
                                                        std::to_string(timeoutMS));
                SharedMemoryPtr sharedMemory = std::make_unique<SharedMemory>(SHARED_MEMORY_BLUETOOTH_SEND_NAME, SharedMemory::READ, true);
                while (gattlibScanProcess.running())
                {
                    if (sharedMemory->dataAvailable())
                    {
                        std::vector<char> d = sharedMemory->pop();

                        // Splitting data into 'macAddress' and 'deviceName' (seperated by '\t')
                        std::vector<std::string> splits;
                        std::istringstream stream(std::string(d.begin(), d.end()));
                        std::string s;
                        while(getline(stream, s, '\t'))
                        {
                            splits.push_back(s);
                        }
                        CHECK(splits.size() == 2);
                        std::string& macAddr = splits[0];
                        std::string& name = splits[1];

                        // Constructing HandDevice
                        for (const HandDevice& d : HandInfo)
                        {
                            if (d.macAdress == macAddr)
                            {
                                HandDevice dc = d;
                                dc.name = name;
                                dc.hardwareTarget = HardwareTarget::Bluetooth;
                                devices.push_back(dc);
                            }
                        }
                    }
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }
                gattlibScanProcess.wait();
                sharedMemory->cleanup();
            }
            catch(boost::exception& e)
            {
                LOG_WARN << boost::diagnostic_information(e);;
            }
            catch(...)
            {
                LOG_WARN << boost::diagnostic_information(boost::current_exception());
            }
            _scanActive = false;
        }
        return devices;
    }

    void KITHandGattlibDriver::receiveLoop()
    {
        while (!_receiveThreadStop)
        {
            if (_gattlibConnectionProcess.valid() && _gattlibConnectionProcess.running())
            {
                if (_sharedMemoryConnectionState->dataAvailable())
                {
                    setConnectionState(static_cast<State>(_sharedMemoryConnectionState->pop()[0]));
                }
                if (_sharedMemoryBluetoothReceive->dataAvailable())
                {
                    _dataCallback(_sharedMemoryBluetoothReceive->pop());
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    std::future<std::vector<HandDevice>> KITHandGattlibDriver::scanForDevices(unsigned int timeoutMS)
    {
        return std::async(std::launch::async, [this, timeoutMS]{ return scanWorker(timeoutMS);});
    }

    void KITHandGattlibDriver::registerDataAvaiableCallback(std::function<void (const std::vector<char>&)> callback)
    {
        _dataCallback = callback;
    }

    void KITHandGattlibDriver::registerConnectionStateChangedCallback(std::function<void (const State)> callback)
    {
        _connectionChangedCallback = callback;
    }

    void KITHandGattlibDriver::sendRaw(const std::vector<char> raw)
    {
        auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - _timestampLastSendCall);
        if (diff < getMinimalTimeBetweenSendCommands())
        {
            return;
        }

        LOG_DEBUG << "Sending raw: '" << std::string(raw.begin(), raw.end()) << "'";

        // we need to split any raw input into packets of max. 20 characters (longer packets may crash the bluetooth device)
        auto iter = raw.begin();
        while (iter != raw.end())
        {
            auto begin = iter;
            iter = (std::distance(iter, raw.end()) > 20) ? iter + 20 : raw.end();
            auto end = iter;
            _sharedMemoryBluetoothSend->push(std::vector<char>(begin, end));
        }
        _timestampLastSendCall = std::chrono::high_resolution_clock::now();
    }

    std::chrono::milliseconds KITHandGattlibDriver::getMinimalTimeBetweenSendCommands()
    {
        return std::chrono::milliseconds(1000 / MAX_SEND_FREQUENCY);
    }

    void KITHandGattlibDriver::setConnectionState(const State state)
    {
        _currentConnectionState = state;
        _connectionChangedCallback(state);
        if (ConnectionState::IsError(state))
        {
            _receiveThreadStop = true;
            _disconnect();
        }
    }

    bool KITHandGattlibDriver::isBluetoothAvailable() const
    {
        std::string cmd = "hciconfig";
        std::array<char, 128> buffer;
        std::string result;
        std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
        if (!pipe) {
            throw std::runtime_error("popen() failed!");
        }
        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
            result += buffer.data();
        }
        return !result.empty();
    }

    void KITHandGattlibDriver::_disconnect()
    {
        _sharedMemoryBluetoothReceive->cleanup();
        _sharedMemoryBluetoothSend->cleanup();
        _sharedMemoryConnectionState->cleanup();
        _sharedMemoryStopProcess->cleanup();
    }
}
