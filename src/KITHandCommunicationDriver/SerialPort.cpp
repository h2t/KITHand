#include "SerialPort.h"
#include "core/Logging.h"
#include <dirent.h>

#include <chrono>
#include <string.h>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <unistd.h> // write(), read(), close()
#include <sys/file.h> // flock()
#include <sys/ioctl.h> // ioctl

#define SERIAL_BAUDRATE B115200

namespace KITHand
{
    SerialPort::SerialPort(const std::string& portName) : _portName(portName)
    {
        std::string error;
        if (!configureAndOpenPort(error))
        {
            throw std::system_error(EIO, std::generic_category(), error);
        }
    }

    SerialPort::~SerialPort()
    {
        closePort();
    }

    void SerialPort::closePort()
    {
        if (_serialPortDescriptor != -1)
        {
            tcsetattr(_serialPortDescriptor, TCSANOW, &_portConfigurationBackup);
            tcflush(_serialPortDescriptor, TCOFLUSH);
            tcflush(_serialPortDescriptor, TCIFLUSH);
            flock(_serialPortDescriptor, LOCK_UN);
            ioctl(_serialPortDescriptor, TIOCNXCL);
            close(_serialPortDescriptor);
            _serialPortDescriptor = -1;
        }
    }

    bool SerialPort::configureAndOpenPort(std::string& error)
    {
        struct termios portConfiguration;
        closePort();

        _serialPortDescriptor = open(("/dev/" + _portName).c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
        if (_serialPortDescriptor == -1)
        {
            error = "While open port " + _portName + ": " + strerror(errno);
            return false;
        }

        // Stop other process from opening same port and in addition aquires an advisory lock
        ioctl(_serialPortDescriptor, TIOCEXCL);
        if (flock(_serialPortDescriptor, LOCK_EX | LOCK_NB) == -1)
        {
            closePort();
            error = "Cannot lock port " + _portName + "! It may currently be in use by another program.";
            return false;
        }

        tcgetattr(_serialPortDescriptor, &portConfiguration);
        // Save old configuration
        memcpy(&_portConfigurationBackup, &portConfiguration, sizeof (struct termios));

        // Bits per Byte
        portConfiguration.c_cflag |= CS8;
        // Turn on READ and ignore ctrl lines (no flow control)
        portConfiguration.c_cflag |= CREAD | CLOCAL;
        portConfiguration.c_cflag &= ~CRTSCTS;
        // Ignore BREAK condition on input and ignore framing and parity errors (copied from https://github.com/Jeija/gtkterm/blob/master/src/serial.c)
        portConfiguration.c_iflag = IGNPAR | IGNBRK;
        // Disable canonical mode (which would need a new line charakter to process data), echo and signal chars
        portConfiguration.c_lflag = 0;
        // Disable any special handling of output bytes
        portConfiguration.c_oflag = 0;
        // Disable any special handling of input bytes
        portConfiguration.c_iflag = 0;

        portConfiguration.c_cc[VTIME] = 0;
        portConfiguration.c_cc[VMIN] = 0;

        // Set input and output speed
        portConfiguration.c_cflag = SERIAL_BAUDRATE;

        if (tcsetattr(_serialPortDescriptor, TCSANOW, &portConfiguration) != 0)
        {
            error = "While tcsetattr on port " + _portName + ": " + strerror(errno);
            return false;
        }
        tcflush(_serialPortDescriptor, TCOFLUSH);
        tcflush(_serialPortDescriptor, TCIFLUSH);

        return true;
    }

    int SerialPort::readFromPort(char* buffer, unsigned int expectedAmountOfBytes, unsigned int timeoutMS) const
    {
        unsigned int receivedBytes = 0;
        char tempBuffer[100];
        unsigned int timePassed = 0;
        while (receivedBytes < expectedAmountOfBytes && timePassed < timeoutMS)
        {
            const auto scanBegin = std::chrono::high_resolution_clock::now();

            memset(&tempBuffer, '\0', sizeof (tempBuffer));
            // We expect, that the serial port is set up with c_cc[VTIME] = 0 and c_cc[VMIN] = 0 --> non-blocking call
            long numBytes = read(_serialPortDescriptor, &tempBuffer, sizeof(tempBuffer));
            if (numBytes < 0)
            {
                if (errno != 11)
                {
                    LOG_WARN << "SerialPort-Error while reading from port: " << strerror(errno);
                    return -1;
                }
            }
            else
            {
                // Make sure we only copy enough bytes to satisfy 'expectedAmountOfBytes'
                unsigned int copyNum = (receivedBytes + numBytes > expectedAmountOfBytes) ? expectedAmountOfBytes - receivedBytes : static_cast<unsigned int>(numBytes);

                memcpy(&buffer[receivedBytes], tempBuffer, copyNum);
                receivedBytes += copyNum;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            const auto scanEnd = std::chrono::high_resolution_clock::now();
            timePassed += std::chrono::duration_cast<std::chrono::milliseconds>(scanEnd-scanBegin).count();
        }
        return static_cast<int>(receivedBytes);
    }

    int SerialPort::readFromPortImmediatly(char *buffer) const
    {
        char tempBuffer[100];
        memset(&tempBuffer, '\0', sizeof (tempBuffer));
        long numBytes = read(_serialPortDescriptor, &tempBuffer, sizeof(tempBuffer));
        if (numBytes < 0)
        {
            LOG_WARN << "SerialPort-Error while reading from port: " << strerror(errno);
            return -1;
        }
        else
        {
            memcpy(buffer, tempBuffer, static_cast<unsigned int>(numBytes));
            return static_cast<int>(numBytes);
        }
    }

    int SerialPort::writeToPort(const char *data, unsigned int length) const
    {
       ssize_t r = write(_serialPortDescriptor, data, length);
       if (r == -1)
       {
           LOG_WARN << "SerialPort-Error: Could not successfully write to port "  << _portName;
       }
       return static_cast<int>(r);
    }

    std::string SerialPort::getPortName() const
    {
        return _portName;
    }

    std::vector<std::string> SerialPort::getAvailablePortsWithPrefix(const std::string &prefix)
    {
        // We scan through /sys/class/tty and check every tty-device if it starts with the given prefix
        std::vector<std::string> result = std::vector<std::string>();
        struct dirent ** namelist;
        int n = scandir("/sys/class/tty", &namelist, nullptr, alphasort);
        if (n < 0)
        {
            LOG_WARN << "Scanning directory /sys/class/tty failed!";
        }
        else
        {
            while (n--)
            {
                std::string entry(namelist[n]->d_name);
                if (entry.compare(0, prefix.size(), prefix) == 0)
                {
                    result.push_back(entry);
                }
                free(namelist[n]);
            }
            free(namelist);
        }
        return result;
    }
}
