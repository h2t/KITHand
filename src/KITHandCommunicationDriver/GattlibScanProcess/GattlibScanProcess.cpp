#include "GattlibScanProcess.h"
#include "core/Logging.h"
#include "HandInfo.h"
#include "SharedMemory.h"
#include <gattlib.h>

#include <chrono>
#include <future>

namespace KITHand
{
    void scan_deviceDiscoveredCallback(void* adapter, const char *addr, const char *name, void* user_data)
    {
        std::vector<std::pair<std::string, std::string>>* list = reinterpret_cast<std::vector<std::pair<std::string, std::string>>*>(user_data);

        std::string macAddr = std::string(addr);
        std::string deviceName;
        try {
            deviceName = std::string(name);
        } catch (...) { }

        // Check if device already detected
        bool deviceAlreadyDiscovered = false;
        for (const auto& d : *list)
        {
            if (d.first == macAddr)
            {
                deviceAlreadyDiscovered = true;
                break;
            }
        }

        // add device if new and mac-adress is known as one of the KIT-hands
        if (!deviceAlreadyDiscovered)
        {
            for (const HandDevice& d : HandInfo)
            {
                if (d.macAdress == macAddr)
                {
                    (*list).emplace_back(macAddr, deviceName);
                    break;
                }
            }
        }
    }


    void GattlibScanProcess::scan(unsigned int timeoutMS)
    {
        // Connecting to shared memory
        SharedMemoryPtr sharedMemorySend;
        try
        {
            sharedMemorySend = std::make_unique<SharedMemory>(SHARED_MEMORY_BLUETOOTH_SEND_NAME, SharedMemory::WRITE);
        }
        catch (const boost::interprocess::interprocess_exception& e)
        {
            LOG_WARN << "Interprocess-Exception: " << e.what() << std::endl;
            return;
        }

        // Run scan
        void* _gattAdapter;
        const char* adapter_name_ptr = nullptr;
        if (gattlib_adapter_open(adapter_name_ptr, &_gattAdapter))
        {
            LOG_WARN << "Failed to open standard bluetooth adapter -- Make sure bluetooth is activated!!";
            return;
        }

        std::vector<std::pair<std::string, std::string>> scan_discoveredDevices = std::vector<std::pair<std::string, std::string>>();
        // If there is less than one second left for the loop, then the loop is exite
        // (workaround for the fact that gattlib_adapter_scan_enable only takes seconds as timeout-parameter)
        const auto scanBegin = std::chrono::high_resolution_clock::now();
        while (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now()-scanBegin).count() <= timeoutMS - 1000)
        {
            if (gattlib_adapter_scan_enable(_gattAdapter, &scan_deviceDiscoveredCallback, 1, reinterpret_cast<void*>(&scan_discoveredDevices)))
            {
                LOG_WARN << "Failed to scan on standard bluetooth adapter";
                gattlib_adapter_scan_disable(_gattAdapter);
                return;
            }
            gattlib_adapter_scan_disable(_gattAdapter);
        }

        // Sending found mac-addresses via shared memory
        if (!scan_discoveredDevices.empty())
        {
            for (const auto& d : scan_discoveredDevices)
            {
                std::vector<char> data(d.first.begin(), d.first.end());
                data.push_back('\t');
                data.insert(data.end(), d.second.begin(), d.second.end());
                sharedMemorySend->push(data);
            }
        }
    }
}
