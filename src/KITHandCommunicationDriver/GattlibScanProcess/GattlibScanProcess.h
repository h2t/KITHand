#pragma once

namespace KITHand
{
    /**
     * @brief Represents the scan process via Bluetooth LE using the gattlib library
     *
     * Gattlib unfortunatly cannot scan from a thread that is not the main thread in a process.
     */
    class GattlibScanProcess
    {
    public:
        /**
         * @brief Scan for hands vie Bluetooth LE using gattlib until the given timeout is reached.
         * @param timeoutMS The timeout in milliseconds
         */
        void static scan(unsigned int timeoutMS);
    };
}
