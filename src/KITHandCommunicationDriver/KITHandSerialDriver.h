#pragma once

#include "KITHandCommunicationDriverBase.h"
#include "SerialPort.h"

#include <thread>
#include <atomic>

namespace KITHand
{
    /**
     * @brief Implementation of KITHandCommunicationDriverBase for communication over serial ports
     */
    class KITHandSerialDriver : KITHandCommunicationDriverBase
    {
    public:
        ~KITHandSerialDriver() override;
        void connect(HandDevice &device) override;
        void disconnect() override;
        std::future<std::vector<HandDevice>> scanForDevices(unsigned int timeoutMS) override;
        void sendRaw(const std::vector<char> raw) override;
        void registerDataAvaiableCallback(std::function<void (const std::vector<char>&)> callback) override;
        void registerConnectionStateChangedCallback(std::function<void(const State)> callback) override;
        std::chrono::milliseconds getMinimalTimeBetweenSendCommands() override;

    private:

        void readLoop();
        SensorProtocol detectSensorProtocol(const std::vector<char>& data) const;
        std::string detectMACAddress(const std::vector<char>& data) const;
        void setConnectionState(State state);

        std::chrono::time_point<std::chrono::high_resolution_clock> _timestampLastSendCall = std::chrono::high_resolution_clock::now();
        std::vector<HandDevice> scan(unsigned int timeoutMS);

        std::function<void (const std::vector<char>&)> _dataCallback;
        std::function<void (const State)> _connectionChangedCallback;
        State _currentConnectionState;

        std::thread _scanThread;
        std::atomic_bool _scanRunning {false};
        std::atomic_bool _scanningAllowed {true};

        std::unique_ptr<SerialPort> _currentPort;
        std::thread _readWriteThread;
        std::atomic<bool> _readWriteThreadStop {false};
    };

    /**
     * @brief UniquePtr to a KITHandSerialDriver
     */
    typedef std::unique_ptr<KITHandSerialDriver> KITHandSerialDriverPtr;
}
