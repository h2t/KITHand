#pragma once

#include <KITHandGattlibDriver.h>
#include <KITHandSerialDriver.h>

#include <atomic>

/**
 * Toplevel namespace of this project
 */
namespace KITHand
{
    /**
     * This namespace encapsulates global parameters that represents limits for various commands that are sent to connected hands
     */
    namespace ControlOptions
    {
        constexpr std::uint64_t maxGraspId      = 8; /**< Maximal indentification number for predefined grasps */
        constexpr std::uint64_t minVelocity   = 10; /**< Minimal velocity in ticks per second for motor commands */
        constexpr std::uint64_t maxVelocity   = 200; /**< Maximal velocity int ticks per second for motor commands */
        constexpr std::uint64_t maxPWM        = 2999; /**< Maximal PWM for motor commands */
        constexpr std::uint64_t maxPosThumb   = 100000; /**< Maximal position in ticks for commands to the thumb motor */
        constexpr std::uint64_t maxPosFingers = 200000; /**< Maximal position in ticks for commands to the fingers motor */
    }

/**
 * @brief Represents a meta driver which encapsulates different drivers for different connection types.
 *
 * This class handles any HandDevice with any combination of KITHand::SensorProtocol and KITHand::ControlProtocol.
 * Furthermore it handles communication over Bluetooth LE as well as a serial port.
 * It provides an interface for writing highlevel control software.
 */
class KITHandCommunicationDriver
    {
    public:
        /**
         * @brief The communication type used for scanning for available hands
         */
        enum ScanMode
        {
            Bluetooth,
            Serial,
            Both
        };

        /**
         * @brief Represents a connection
         */
        struct Connection
        {
            HandDevice device; /**< The HandDevice handled by this connection */
            State state; /**< The current state of this connection */
        };

        /**
         * @brief Constructs a new KITHandCommunicationDriver instance, instantiates a KITHandSerialDriver and a KITHandGattlibDriver
         * and registers callbacks.
         *
         * Will only have a KITHandGattlibDriver if a bluetooth module is available on the system.
         */
        KITHandCommunicationDriver();

        /**
         * @brief Connects to the hand represented by the given \p device via the appropriate driver
         * @param device The HandDevice to connect to
         * @return true if the connection was successful and false otherwise
         */
        bool connect(HandDevice& device);
        /**
         * @brief Disconnects from the current hand
         */
        void disconnect();
        /**
         * @brief Reconnects to the last device this isntance was connected to.
         * @return true if reconnecting was successful
         */
        bool reconnect();
        /**
         * @return true if this instance is currently connected to a hand
         */
        bool connected() const;
        /**
         * @return the current KITHand::ConnectionState::State
         */
        State getCurrentConnectionState() const;
        /**
         * @return a pointer to the struct representing the currently connected hand
         */
        HandDevice* getCurrentConnectedDevice() const;
        /**
         * @brief Registers a callback which will be called whenever the state of the current connection changes.
         * @param callback The callback to register
         */
        void registerConnectionStateChangedCallback(std::function<void(const State)> callback);
        /**
         * @brief Scans for hands on the communication channels defined by \p mode.
         *
         * This is a blocking call and waits until all scan-calls of the specialized drivers are finished.
         *
         * @param mode The scan mode
         * @param scanDuration The maximal scan duration
         * @return a list of detected hands
         */
        std::vector<HandDevice> scanForDevices(ScanMode mode = ScanMode::Both, unsigned int scanDuration = 1000) const;
        /**
         * @brief Scans asynchronous for hands on the communication channels defined by \p mode.
         *
         * @param mode The scan mode
         * @param scanDuration The maximal scan duration
         * @return a future to a list of detected hands
         */
        std::future<std::vector<HandDevice>> scanForDevicesAsync(ScanMode mode = ScanMode::Both, unsigned int scanDuration = 1000) const;

        /**
         * @brief Waits until the underlying communication medium can be expected to be ready again.
         *
         * Every driver has a maximal frequency with which new data packages can be send. This frequency depends on the supported datarate
         * of the medium (for example bluetooth or serial communication).
         */
        void waitForCommunicationMedium() const;

        //sense
        /**
         * @return the pwm value of the thumb motor
         */
        std::int64_t getThumbPWM() const;
        /**
         * @return the position of the thumb motor in motor ticks
         */
        std::int64_t getThumbPos() const;
        /**
         * @return the pwm value of the fingers motor
         */
        std::int64_t getFingersPWM() const;
        /**
         * @return the position of the fingers motor in motor ticks
         */
        std::int64_t getFingersPos() const;
        /**
         * @return the roll value of the IMU
         */
        std::int64_t getIMURoll() const;
        /**
         * @return the pitch value of the IMU
         */
        std::int64_t getIMUPitch() const;
        /**
         * @return the yaw value of the IMU
         */
        std::int64_t getIMUYaw() const;
        /**
         * @return the distance measured by the time of flight sensor
         */
        std::int64_t getTimeOfFlight() const;
        //control
        /**
         * @brief Overloadad version of sendRaw(std::vector<char> raw) const for convenient sending of data that does not contain '\0'.
         * @param raw The data to send
         */
        void sendRaw(std::string raw) const;
        /**
         * @brief Sends the given data to the currently connected hand via the appropriate driver. If no hand is currently connected, nothing happens.
         * @param raw The data to send
         */
        void sendRaw(std::vector<char> raw) const;
        /**
         * @brief sends graspId to hand
         *
         * Sends the given graspId for predefined grasps to the currently connected hand.
         * The given graspId cannot be larger than KITHand::ControlOptions::maxGraspId, otherwise an exception std::invalid_argument is thrown.
         * GraspId 0 opens the hand, graspId 1 closes the hand.
         * @param g The given graspId (between 0 and KITHand::ControlOptions::maxGraspId)
         */
        void sendGrasp(std::uint64_t g) const;
        /**
         * @brief Sends a position command for the thumb motor to the hand
         * @param v The maximal velocity of the thumb motor in ticks per second (between 0 and KITHand::ControlOptions::maxVelocity)
         * @param pwm The maximal PWM of the thumb motor (between 0 and KITHand::ControlOptions::maxPWM)
         * @param pos The target position of the thumb motor in motor ticks (between 0 and KITHand::ControlOption::maxPosThumb)
         * @throws std::invalid_argument if any parameter is outside of its range defined by KITHand::ControlOptions
         */
        void sendThumbPosition(std::uint64_t pos, std::uint64_t v = ControlOptions::maxVelocity, std::uint64_t pwm = ControlOptions::maxPWM) const;
        /**
         * @brief Sends a position command for the fingers motor to the hand
         * @param v The maximal velocity of the fingers motor in ticks per second (between 0 and KITHand::ControlOptions::maxVelocity)
         * @param pwm The maximal PWM of the fingers motor (between 0 and KITHand::ControlOptions::maxPWM)
         * @param pos The target position of the fingers motor in motor ticks (between 0 and KITHand::ControlOption::maxPosFingers)
         * @throws std::invalid_argument if any parameter is outside of its range defined by KITHand::ControlOptions
         */
        void sendFingersPosition(std::uint64_t pos, std::uint64_t v = ControlOptions::maxVelocity, std::uint64_t pwm = ControlOptions::maxPWM) const;
        /**
         * @brief Sends a giver \p command together with the given \p input to the connected hand.
         *
         * This function does nothing if the current connection is not via Bluetooth LE.
         * Commands send with this function are preprocessed by the firmware on the hand and sent more or less directly as an AT-command to the
         * bluetooth module on the hand with the aim to configure it. <br>
         * For stability reasons only a very limited set of this special commands are supported. Currently only the changing of the advertising
         * name of the bluetooth module is implemented.
         *
         * @param command The command to send
         * @param input The (optional) input data for the command
         */
        void sendPseudoATCommand(const char& command, const std::string input) const;
        /**
         * Opens the currently connected hand.
         */
        void openHand() const;
        /**
         * Closes the currently connected hand.
         */
        void closeHand() const;

private:
        void sendPWM(std::uint64_t v, std::uint64_t pwm, std::uint64_t pos, std::uint64_t max_pos, const std::string& name, std::uint64_t motor) const;
        void dataReceivedCallback(const std::vector<char>& data);
        void connectionStateChangedCallback(const State newState);
        void parseSensorValueLine(const std::string& line);
        void parseSensorValueBinary(const std::vector<char>& line, char version);
        void parseSensorValue(const std::vector<char>& data);

        std::vector<char>                   _processBuffer;

        std::unique_ptr<Connection>         _currentConnection;
        std::function<void (const State)>   _connectionChangedCallback;

        std::atomic_int64_t                 _thumbPWM{0};
        std::atomic_int64_t                 _thumbPos{0};
        std::atomic_int64_t                 _fingersPWM{0};
        std::atomic_int64_t                 _fingersPos{0};
        std::atomic_int64_t                 _IMUroll{0};
        std::atomic_int64_t                 _IMUpitch{0};
        std::atomic_int64_t                 _IMUyaw{0};
        std::atomic_int64_t                 _timeOfFlight{0};

        KITHandGattlibDriverPtr             _bluetoothDriver;
        KITHandSerialDriverPtr              _serialDriver;
    };

/**
 * @brief SharedPtr to a KITHandCommunicationDriver
 */
typedef  std::shared_ptr<KITHandCommunicationDriver> KITHandCommunicationDriverPtr;
}
