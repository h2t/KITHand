#include "Logging.h"
#include "g3log/logworker.hpp"
#include <mutex>

#include <iostream>

namespace KITHand
{
    struct ColoredCoutSink
    {
        enum FG_Color {YELLOW = 33, RED = 31, GREEN = 32, WHITE = 97};

        FG_Color GetColor(const LEVELS level) const
        {
            if (level.value == WARNING.value) { return YELLOW; }
            if (level.value == DEBUG.value) { return GREEN; }
            if (g3::internal::wasFatal(level)) { return RED; }

            return WHITE;
        }

        void ReceiveLogMessage(g3::LogMessageMover logEntry)
        {
            auto level = logEntry.get()._level;
            auto color = GetColor(level);

            std::string s;
            s = logEntry.get().timestamp("%H:%M:%S,%f3") + " [" +
                logEntry.get().file() + "->" +
                logEntry.get().function() + ':' +
                logEntry.get().line() + " ]\t" +
                logEntry.get().message() + "\n";

            std::cout << "\033[" << color << "m" << s << "\033[m";
        }
    };

    static std::once_flag initialized;

    void Logging::initialize()
    {
        std::call_once(initialized, [](){
            using namespace g3;
            static std::unique_ptr<LogWorker> logworker{ LogWorker::createLogWorker() };
            static auto sinkHandle = logworker->addSink(std::make_unique<ColoredCoutSink>(), &ColoredCoutSink::ReceiveLogMessage);
            initializeLogging(logworker.get());
        });
    }

    void Logging::setFatalHook(std::function<void ()> fatalHook)
    {
        g3::setFatalPreLoggingHook(fatalHook);
    }
}
