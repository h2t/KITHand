#pragma once

#include <string>
#include <vector>
#include <termios.h> // Contains POSIX terminal control definitions

namespace KITHand
{
    /**
     * @brief Represents a wrapper around a serial port and allows for reading and writing data from and to the port.
     */
    class SerialPort
    {
    public:
        /**
         * @brief Tries to open and to configure a serial port with the given name.
         *
         * Checks if a serial port with the given name exists, opens it and locks it. Therefore no other process or thread can
         * access that port until this instance of SerialPort is destroyed. <br>
         * After opening the port it is configured according to following settings:
         * Setting | Value
         * ----|------
         * baud rate | 115200
         * data bits | 8
         * stop bit | 1
         * parity | none
         *
         * The previous settings are saved to be able to restore them in the destructor of this class.
         *
         * @throws std::system_error if configuring the port failed
         * @param portName The name of the serial port
         */
        SerialPort(const std::string& portName);

        /**
          * @brief Restores the opened port to its previous configuration and closes it afterwards.
          */
        ~SerialPort();

        /**
         * @brief Reads bytes from the serial port until either \p expectedAmountOfBytes bytes have been read or the timeout ran out.
         *
         * Checks periodically the serial port for available bytes and writes them to the given \p buffer. This repeats until
         * either the amount of read bytes is equal to \p expectedAmountOfBytes or the given amount of milliseconds has passed.
         *
         * @param buffer The buffer where the read bytes are written to.
         * @param expectedAmountOfBytes The amount of bytes that must be read from the serial port.
         * @param timeoutMS The timeout in milliseconds.
         * @return the actual amount of bytes read. -1 if an error occured during reading
         */
        int readFromPort(char* buffer, unsigned int expectedAmountOfBytes, unsigned int timeoutMS) const;

        /**
         * @brief Reads all currently available data from the serial port and writes it to the given buffer.
         * @param buffer The buffer where the available bytes are written to.
         * @return the amount of bytes read
         */
        int readFromPortImmediatly(char* buffer) const;
        /**
         * @brief Writes data to the serial port opened by this instance.
         *
         * @param data Pointer to byte array with the data to send
         * @param length The amount of bytes to send
         *
         * @return the amount of bytes that have actually been written to the port. -1 if an error occured during writing.
         */
        int writeToPort(const char* data, unsigned int length) const;
        /**
         * @return the name of this serial port
         */
        std::string getPortName() const;

        /**
         * @brief Returns a vector with the names of the available ports that start with the given prefix.
         *
         * @param prefix The prefix which determines if a port is returned.
         * @return a list of port names with the given prefix
         */
        static std::vector<std::string> getAvailablePortsWithPrefix(const std::string& prefix);
    private:
        void closePort();
        bool configureAndOpenPort(std::string& error);

        std::string _portName;
        int _serialPortDescriptor = -1;
        struct termios _portConfigurationBackup;
    };
}


